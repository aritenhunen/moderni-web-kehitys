<?php $kayttaja = false; ?>
<?php require_once("header.php"); ?>

<main>
<p>Tervetuloa käyttämään MuistiApuri-palvelua. Täällä voit säilyttää keskitetysti kaikki merkinnät muistettavista asioista ja jakaa niitä tarvittaessa muillekin käyttäjille.</p>

<?php if ($kayttaja==false) { ?>
<p>Etkö ole vielä käyttäjä? <a href="register.php">Rekisteröidy nyt!</a></p>
<?php } ?>
</main>
<?php

require_once("sidebar.php");
require_once("footer.php");
?>