<?php require_once("header.php"); ?>
<?php require_once("yhteys.php"); ?>
<?php
$muistilista_virhe = "";
$muistilista_onnistui = "";
$muistettavaID = "";
$note_title = "";
$note_desc = "";
$note_category = "";
$note_deadline = "";
$note_done = "";
$category_id = "";
$category_name = "Valitse kategoria...";
$note_done = "";
$kayttaja = kayttaja();

	
if ($kayttaja==true) {

if (isset($_GET['modifynote']) && ($_GET['modifynote']  !== "")) {
	$muistettavaID = strip_tags($_GET["modifynote"]);
}

if (isset($_GET['noteid']) && ($_GET['noteid']  !== "")) {

	$muistettavaID = strip_tags($_GET["noteid"]);
	
}

if (isset($_GET['removeshare']) && ($_GET['removeshare']  !== "")) {

	$muistettavaID = strip_tags($_GET["removeshare"]);
	
}

if (isset($_GET['deletenote']) && ($_GET['deletenote']  !== "")) {

	$muistettavaID = strip_tags($_GET["deletenote"]);
	
}

	$kayttaja = kayttaja();
	
	
if ($kayttaja==true) {
		
function tarkistaMuokkausOikeus($muistettavaID){

	$yhteys = yhteystietokantaan();
	$tarkistus = array();
	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);	
	$tulos = $yhteys->prepare("SELECT note.note_id, note.done, note.title, category.name, note.deadline, note.description, category.owner, user.nickname FROM ((note LEFT JOIN category ON note.category = category.category_id) LEFT JOIN user ON user.username = category.owner) WHERE category.owner = '$kayttaja_sessio' AND note.note_id = '$muistettavaID' LIMIT 1");
	$tulos->execute();
	$res = $tulos->get_result();
	
	if($res->num_rows !== 0) {
		
	while ($row= mysqli_fetch_array($res)){
		$tarkistus[] = $row;
	}
		return $tarkistus;
  
	} else {
		
		$tarkistus = false;
		
		return $tarkistus;
	}
}

	$muistettava = tarkistaMuokkausOikeus($muistettavaID);

if(tarkistaMuokkausOikeus($muistettavaID) === false) {

	?> <script> alert("Sinulla ei ole muokkausoikeutta tähän muistettavaan asiaan!"); window.location.href="index.php";</script> <?php
	
} else {
	
	
	if(isset($_GET["deletenote"]) && ($_GET["deletenote"] == $muistettavaID)){
	
		
	$tulos = $yhteys->prepare("DELETE FROM note WHERE note_id = '$muistettavaID'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
	
	header("Location: notes.php?delete2_value=true"); 
	
	
}
	
	
if(isset($_GET["removeshare"]) && ($_GET["removeshare"] == $muistettavaID)){
		
	if(isset($_GET["user"])) {
		
		$poistettava = strip_tags($_GET["user"]);
	
	$tulos = $yhteys->prepare("DELETE FROM share WHERE username='$poistettava' AND note_id = '$muistettavaID'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
	
	header("Location: note-view.php?noteid=" . $muistettavaID ."&delete_value=true"); 
	
	}

	
	
}
	
	


if(isset($_POST['modify-note'])) {
	
	$note_title = $yhteys -> real_escape_string(strip_tags($_POST['note-title'])); 
	$note_desc = $yhteys -> real_escape_string(strip_tags($_POST['note-desc'])); 
	$note_category = $yhteys -> real_escape_string(strip_tags($_POST['note-category'])); 
	$note_deadline = $yhteys -> real_escape_string(strip_tags($_POST['note-deadline'])); 
	
	
	if (isset($_POST['note-done']) == 'value1') {
	
	$note_done = $yhteys -> real_escape_string(strip_tags(1)); 
		
	
	} else {
		
			$note_done = $yhteys -> real_escape_string(strip_tags(0)); 
		
	}

	
	$tulos = $yhteys->prepare("SELECT * FROM note WHERE note_id = '$muistettavaID'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
	
	$row = mysqli_fetch_assoc($res);

	if ($note_title == "") {
	
		$note_title = $row['title'];
	
	}
	
	if ($note_desc == "") {
	
		$note_desc = $row['description'];
	
	}
	
	if ($note_category == "") {
	
		$note_category = $row['category'];
	
	}
	
	if ($note_deadline == "") {
	
		$note_deadline = $row['deadline'];
	
	}
	

if (($note_title !== "") && ($note_desc !== "") && ($note_category !== "") && ($note_deadline !== "")) {
	
	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);

	$tulos = $yhteys->prepare("SELECT * FROM category WHERE category_id='$note_category' and owner='$kayttaja_sessio'");
	$tulos->execute();
	$res500 = $tulos->get_result();
	$tulos -> close();
	
	if($res500->num_rows !== 0) {


	$stmt = $yhteys->prepare("UPDATE note SET title= ?, description= ?, category= ?, deadline= ?, done= ? WHERE note_id = ?");
	$stmt->bind_param("ssisii", $note_title, $note_desc, $note_category, $note_deadline, $note_done, $muistettavaID);
	$stmt->execute();
	$stmt -> close();

		header("Location: note-view.php?noteid=" . $muistettavaID ."&update_value=true");  
	
	} else {
		
		?> <script> alert("Ei kelvollinen kategoria!");</script> <?php 
	}
	
	} else {
	
	?> <script> alert("Päivittämisen yhteydessä tapahtui virhe!");</script> <?php 

}
 
	
}
}
}


	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);
	$yhteys = yhteystietokantaan();
	
	$tulos = $yhteys->prepare("SELECT * FROM category WHERE owner='$kayttaja_sessio'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();


?>
<main>
<h2>Muokkaa muistettavaa asiaa</h2>

<!-- noteid siirrettävä GET-parametrista uuteen linkkiin -->
<form action="note-modify.php?noteid=<?php echo $muistettavaID; ?>" method="post">
	<p><span class="note-modify-label"><b>Nimi: </b></span><input type="text" name="note-title"></p>

	<p><b>Kuvaus</b><br>
	<textarea name="note-desc" rows="5" cols="50"></textarea>
	</p>
	<!-- hae kategoriat tietokannasta -->
	<p><span class="note-modify-label"><b>Kategoria: </b></span>
		<select name="note-category">
			<option value="">Valitse kategoria...</option>
			<?php while($row = mysqli_fetch_assoc($res)) { ?>
		
			<option value="<?php echo $row['category_id']; ?>"><?php echo $row['name']; ?></option>
				
	
			<?php } ?>
		</select>
	</p>
	<p><span class="note-modify-label"><b>Aikaraja: </b></span><input type="date" name="note-deadline"></p>
	<p><span class="note-modify-label"><b>Tehty: </b></span><input type="checkbox" value="value1" name="note-done" <?php if ($note_done == 1) echo "checked='checked'"; ?>></p>
	<p><input type="submit" name="modify-note" value="Tallenna"></p>
</form>
<?php echo $muistilista_virhe; ?>
<?php echo $muistilista_onnistui; ?>
<?php
	$jaa_virhe = "";

	$tulos = $yhteys->prepare("SELECT * FROM user WHERE user.username NOT IN (select username from share WHERE share.note_id = '$muistettavaID') AND user.username NOT LIKE '$kayttaja_sessio'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();

	if((isset($_POST["share-note"]) && ($_POST["share-note"] !== ""))){
		
		$jaa = $yhteys -> real_escape_string(strip_tags($_POST['share-with'])); 
		
		if ($jaa !== "") {
			
		$tulos33 = $yhteys->prepare("SELECT username FROM share WHERE username = '$jaa' AND share.note_id = '$muistettavaID'");
		$tulos33->execute();
		$res33 = $tulos33->get_result();
	
		if($res33->num_rows == 0) {
		
		$stmt = $yhteys->prepare("INSERT INTO share (note_id, username) VALUES (?, ?)");
		$stmt->bind_param("is", $muistettavaID, $jaa);
		$stmt->execute();
		$stmt -> close();
		
		header("Location: note-view.php?noteid=" . $muistettavaID ."&share_value=true"); 
		}
			
		} else {
			
			$jaa_virhe = "<p class='virhe'>Valitse käyttäjä!</p>";
			
		}
	}

?>

<h3>Jaa muistettava asia</h3>

<form action="note-modify.php?noteid=<?php echo $muistettavaID; ?>" method="post">
	<p>
		<!-- hae käyttäjänimet tietokannasta -->
		<select name="share-with">
			<option value="">Valitse käyttäjä...</option>
			<?php while($row = mysqli_fetch_assoc($res)) { ?>
	
			<option value="<?php echo $row['username']; ?>"><?php echo $row['nickname']; ?></option>
				
			
			
			<?php } ?>
		</select>

	<input type="submit" name="share-note" value="Jaa">
	</p>
</form>
<?php echo $jaa_virhe; ?>
</main>
<?php
} else {
	
	?> <script> alert("Käyttäjä ei löytynyt!"); window.location.href="index.php";</script> <?php	
}
require_once("sidebar.php");
require_once("footer.php");
?>