<?php require_once("header.php"); ?>
<?php require_once("yhteys.php"); ?>
<?php
	$profiili_muokkaus_onnistui = "";
	$profiili_muokkaus_epaonnistui = "";
	$salasana_onnistui = "";
	$salasana_epaonnistui = "";
	$kayttaja = kayttaja();
	$yhteys = yhteystietokantaan();

	
if ($kayttaja==true) {
	
	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);
	
	$tulos44 = $yhteys->prepare("SELECT password, username FROM user WHERE username='$kayttaja_sessio'");
	$tulos44->execute();
	$res44 = $tulos44->get_result();
	$kayttaja = mysqli_fetch_assoc($res44);
	
	/* profiilin muokkaus */
	if(isset($_POST["modify-profile"])) {
	
	$nickname = $yhteys -> real_escape_string(strip_tags($_POST['nickname'])); 
	$email = $yhteys -> real_escape_string(strip_tags($_POST['email'])); 


if (($nickname !== "") && ($email !== "")) {
	
	$email = strlen($email);

	$stmt = $yhteys->prepare("UPDATE user SET nickname= ?, email= ? WHERE username = ?");
	$stmt->bind_param("sis", $nickname, $email, $kayttaja_sessio);
	$stmt->execute();
	$stmt -> close();
	
	$profiili_muokkaus_onnistui = "<p id='onnistui'>Profiilin tiedot tallennettiin onnistuneesti!</p>";

	
} else {
	
	$profiili_muokkaus_epaonnistui = "<p class='virhe'>Kentät ovat tyhjät, täytä kentät!</p>";
}
	}
	
		/* salasanan muokkaus */
if(isset($_POST["change-password"])) {
	
	
	$old_password = $yhteys -> real_escape_string(strip_tags($_POST['old-password'])); 
	$salasana = $yhteys -> real_escape_string(strip_tags($_POST['new-password1'])); 
	$salasana2 = $yhteys -> real_escape_string(strip_tags($_POST['new-password2'])); 
		
	
if (($salasana !== "") && ($old_password !== "") && ($salasana2 !== "")) {
	
	if ($salasana === $salasana2) {

	
	if ($kayttaja) {
		
		$old_password = MD5($old_password);
		$salasana = MD5($salasana);
		$kayttajatunnus = $kayttaja['username'];
		
		
	if ($kayttaja['password'] == $old_password) {
	
		$stmt = $yhteys->prepare("UPDATE user SET password= ? WHERE username = ?");
		$stmt->bind_param("ss", $salasana, $kayttajatunnus);
		$stmt->execute();
		$stmt -> close();
		
		$salasana_onnistui = "<p id='onnistui'>Salasanan vaihtaminen onnistui!</p>";
		
	} else {
		
		$salasana_epaonnistui = "<p class='virhe'>Vanha salasana ei täsmää!</p>";
	}
		
	} else {
		
		die("Käyttäjä tietoja ei löytynyt!");
	}
	
	
	} else {
		
		$salasana_epaonnistui = "<p class='virhe'>Salasanat eivät täsmää!</p>";
	}

} else {
	
	$salasana_epaonnistui = "<p class='virhe'>Kentät ovat tyhjät, täytä kentät!</p>";
}
}
?>
<main>

<h2>Omat tiedot</h2>
<!-- kenttiin haetaan tietokannasta käyttäjän tiedot. Tallennettaessa tarkistetaan, etteivät kentät ole tyhjiä. -->

<form action="profile.php" method="post">

	<p><span class="profile-label">Käyttäjätunnus: </span><?php echo $kayttaja['username']; ?></p>

	<p><span class="profile-label">Nimi: </span><input type="text" name="nickname"></p>

	<p><span class="profile-label">Sähköposti: </span><input type="email" name="email"></p>

	<p><input type="submit" name="modify-profile" value="Tallenna"></p>

</form>
<?php echo $profiili_muokkaus_onnistui; ?>
<?php echo $profiili_muokkaus_epaonnistui; ?>

<h3>Vaihda salasana</h3>
<!-- Vanhan salasanan oikeellisuus tulee tarkistaa kannasta. -->
<form action="profile.php" method="post">

	<p><span class="profile-label">Vanha salasana: </span><input type="password" name="old-password"></p>

	<p><span class="profile-label">Salasana: </span><input type="password" name="new-password1"></p>

	<p><span class="profile-label">Salasana uudelleen: </span><input type="password" name="new-password2"></p>

	<p><input type="submit" name="change-password" value="Vaihda salasana"></p>

</form>
<?php echo $salasana_onnistui; ?>
<?php echo $salasana_epaonnistui; ?>
</main>
<?php
$tulos44 -> close();
} else {
	
	?> <script> alert("Käyttäjä ei löytynyt!"); window.location.href="index.php";</script> <?php	
	
}
require_once("sidebar.php");
require_once("footer.php");
?>