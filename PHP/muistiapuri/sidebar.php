<?php require_once("yhteys.php"); ?>
<?php
 
$kirjautuminen = "";
   /* GET parametri kirjautuminen */
 if(isset($_GET["kirjautuminen2"]) && $_GET["kirjautuminen2"] == "tyhja") {  
  
	$kirjautuminen = "<p class='virhe'>Täytä kentät!</p>";
  
 } else  if(isset($_GET["kirjautuminen2"]) && $_GET["kirjautuminen2"] == "vaarin") {  
 
	$kirjautuminen = "<p class='virhe'>Virheellinen käyttäjätunnus ja salasana!</p>";
 
 }

 ?>
<div id="sidebar">
	<div id="login-box">
		<h2>Kirjaudu sisään</h2>
		<div id="login-box-content">
		<?php if(!isset($_SESSION['kayttaja'])) {?>
			<!-- vain ei-kirjautuneena -->
			<!-- login.php näyttää virheilmoituksen jos tunnus / salasana väärin, muuten kirjaa käyttäjän sisään ja ohjaa etusivulle -->
			<form action="login.php" method="post">
				<p><span class="login-label">Tunnus: </span><input type="text" name="username"></p>
				<p><span class="login-label">Salasana: </span><input type="password" name="password"></p>
				<p><input type="submit" name="login-submit" value="Kirjaudu"></p>
			</form>
			<p class="footnote">Ei tunnuksia? <a href="register.php">Rekisteröidy!</a>
		<?php echo $kirjautuminen; } else { ?>
			
			<!-- vain kirjautuneena -->
			<p>Olet kirjautunut: <b><?php echo $_SESSION['kayttaja']; ?></b></p> <!-- sessiosta ja/tai kannasta -->
			<p><a href="login.php?logout=true">Kirjaudu ulos</a></p>
		
		</div>
	</div>
	<?php
	
	$yhteys = yhteystietokantaan();
	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);
	$tulos = $yhteys->prepare("SELECT share.username, category.owner, note.deadline, note.note_id, note.description, user.nickname FROM note LEFT JOIN category ON category.category_id = note.category LEFT JOIN share ON share.note_id = note.note_id AND share.username = '$kayttaja_sessio' LEFT Join user On category.owner = user.username AND category.category_id = note.category WHERE DATEDIFF(deadline, CURRENT_DATE) < 7 AND done LIKE '0' AND (user.username = '$kayttaja_sessio' or share.username = '$kayttaja_sessio') ORDER BY deadline");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
	
	?>
	<!-- vain kirjautuneena -->
	<div id="urgent-notes">
	<?php if($res->num_rows !== 0) { ?>
		<h2>Kiireelliset asiat</h2>
			<div id="urgent-notes-content">
	<?php } else {?>

		<h2>Ei kiireisiä asioita.</h2>
	
	<?php } ?>
		<!-- Tämän sisältö dynaamisesti tietokannasta, kaikki muistettavat asiat joiden deadline on alle 7 päivän sisällä ja jotka tekemättä. Kiireisimmät ylimpänä. Jos ei yhtäkään, teksti "Ei kiireisiä asioita." -->
	
			<?php while($row = mysqli_fetch_assoc($res)) { ?>
			<?php $paivamaara = date("d-m-Y ", strtotime($row['deadline'])); ?>
			<div class="urgent-note">
				<span class="deadline"><?php echo $paivamaara; ?></span>
				<span class="topic"><b><a href="note-view.php?noteid=<?php echo $row['note_id']; ?>"><?php echo $row['description']; ?></a></b></span>
				<?php if($row['username'] !== null && $row['owner'] !== "$kayttaja_sessio") {?>
				<span class="shared-by"><?php echo $row['nickname'] ?></span>
				<?php }?> 
			</div>
			<?php }?>
	<?php if($res->num_rows !== 0) { ?>	
		</div>
	<?php }?>
	</div>
</div>
<?php } ?>