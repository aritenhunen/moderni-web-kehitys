<?php require_once("header.php"); ?>
<?php require_once("yhteys.php"); ?>
<?php

	$muistilista_virhe = "";
	$muistilista_onnistui = "";
	$category_name = "Valitse kategoria...";
	$note_desc = "";
	$note_deadline = "";
	$note_title = "";
	$category_id = "";


	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);
	$yhteys = yhteystietokantaan();
	
	$tulos = $yhteys->prepare("SELECT * FROM category WHERE owner='$kayttaja_sessio'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
	$kayttaja = kayttaja();

	
if ($kayttaja==true) {
			
if(isset($_POST["add-note"])) {
	
	$note_title = $yhteys -> real_escape_string(strip_tags($_POST['note-title'])); 
	$note_desc = $yhteys -> real_escape_string(strip_tags($_POST['note-desc']));
	$note_category = $yhteys -> real_escape_string(strip_tags($_POST['note-category'])); 
	$note_deadline = $yhteys -> real_escape_string(strip_tags($_POST['note-deadline']));	


 	if((isset($_POST["note-category"]) && ($_POST["note-category"] !== ""))){
	
	$tulos = strip_tags($_POST["note-category"]);
	
	$arvot = explode(" ", $tulos);
	
	if (!isset($arvot[1])) {
   $arvot[1] = null;
	}
	
	if (!isset($arvot[0])) {
   $arvot[0] = null;
	}
 
	
		$category_id = $arvot[0];
			
		$category_name = $arvot[1];
	


	if ($category_name == "Valitse") {
		
		$category_name = "Valitse kategoria...";
		
	}
 
	
} 

	



if (($note_title !== "") && ($note_desc !== "") && ($note_category !== "") && ($note_deadline !== "") && ($category_name !== "Valitse kategoria...")) {
	
	$valmis = 0;
	
	$tulos = $yhteys->prepare("SELECT category_id FROM category WHERE category_id='$note_category'");
	$tulos->execute();
	$res500 = $tulos->get_result();
	$tulos -> close();
	
	if($res500->num_rows !== 0) {

	$stmt = $yhteys->prepare("INSERT INTO note (title, description, category, deadline, done) VALUES (?, ?, ?, ?, ?)");
	$stmt->bind_param("ssisi", $note_title , $note_desc, $note_category, $note_deadline, $valmis);
	$stmt->execute();
	$stmt -> close();
	
	$muistilista_onnistui = "<p id='onnistui'>Muistilistan lisääminen onnistui!</p>";
	
	$category_name = "Valitse kategoria...";
	$note_desc = "";
	$note_deadline = "";
	$note_title = "";
	$category_id = "";
	
	} else {
		
		$muistilista_virhe = "<p class='virhe'>Kategoria ei ole olemmassa!</p>";
	}
		
} else {
	
	$muistilista_virhe = "<p class='virhe'>Kentät ovat tyhjät, täytä kentät!</p>";

}
 
 


}


 
?>

<main>
<h2>Lisää muistettava asia</h2>

<form action="note-add.php" method="post">
	<p><span class="note-add-label"><b>Nimi: </b></span><input type="text" value="<?php echo $note_title; ?>" name="note-title"></p>

	<p><b>Kuvaus</b><br>
	<textarea name="note-desc" rows="5" cols="50" maxlength="500"><?php echo $note_desc; ?></textarea>
	</p>
	<!-- hae kategoriat tietokannasta -->
	<p><span class="note-add-label"><b>Kategoria:</b></span>
		<select name="note-category">
			<option value="<?php echo $category_id . ' ' . $category_name;  ?>"><?php echo $category_name; ?></option>
			<?php while($row = mysqli_fetch_assoc($res)) { ?>
		
	<?php if($category_name !== $row['name']) {?>
	
			<option value="<?php echo $row['category_id'] . ' ' . $row['name']; ?>"><?php echo $row['name']; ?></option>
				
				<?php } ?>
			
			<?php } ?>
		</select>

	</p>
	<p><span class="note-add-label"><b>Aikaraja: </b></span><input type="date" value="<?php echo $note_deadline; ?>" name="note-deadline"></p>
	<p><input type="submit" name="add-note" value="Tallenna"></p>
</form>
<?php echo $muistilista_virhe; ?>
<?php echo $muistilista_onnistui; ?>
</main>
<?php
} else {
	
	?> <script> alert("Käyttäjä ei löytynyt!"); window.location.href="index.php";</script> <?php	
	
}
require_once("sidebar.php");
require_once("footer.php");
?>