<?php require_once("header.php"); ?>
<?php require_once("yhteys.php"); ?>
<?php require_once("login.php"); ?>
<?php 
	
	$kategoria_virhe = "";
	$kategoria_onnistui = "";
	
	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);
	
	$yhteys = yhteystietokantaan();
	
	/* kategorian lisääminen */
	
	if(isset($_POST["add-category"])) { 
	
		$kategoria_nimi = $yhteys -> real_escape_string(strip_tags($_POST['add-category-name'])); 
	  
		if ($kategoria_nimi !== "") {
			
			$tulos = $yhteys->prepare("SELECT username FROM user WHERE username='$kayttaja_sessio'");
			$tulos->execute();
			$res = $tulos->get_result();
			$tulos -> close();
			
			$kayttaja = mysqli_num_rows($res);
			
			if($kayttaja) {
				
				$tulos = $yhteys->prepare("SELECT * FROM category WHERE name='$kategoria_nimi' and owner='$kayttaja_sessio'");
				$tulos->execute();
				$res = $tulos->get_result();
				$tulos -> close();
				
				if($res->num_rows < 1) {
				
					$stmt = $yhteys->prepare("INSERT INTO category (name, owner) VALUES (?, ?)");
					$stmt->bind_param("ss", $kategoria_nimi , $kayttaja_sessio);
					$stmt->execute();
					$stmt -> close();
					header("Location: manage.php?kategoria_lisaaminen=true");
				
				} else {
					
					header("Location: manage.php?kategoria_lisaaminen=false");
					
				}
			
			}
			
			
		} else {
			
			header("Location: manage.php?kategoria_lisaaminen=tyhja");
			
		}
	}
	
	if(isset($_GET['kategoria_lisaaminen']) && $_GET['kategoria_lisaaminen'] == "tyhja") {
		
		$kategoria_virhe = "<p class='virhe'>Täytä nimi kenttä!</p>";
		
	} else if(isset($_GET['kategoria_lisaaminen']) && $_GET['kategoria_lisaaminen'] == "true") {
		
		$kategoria_onnistui = "<p id='onnistui'>Kategorian lisäämisen onnistui!</p>"; 	
	
	} else if(isset($_GET['kategoria_lisaaminen']) && $_GET['kategoria_lisaaminen'] == "false") {
		
		$kategoria_virhe = "<p class='virhe'>Kategoria on olemassa!</p>";
	
	}
	/* kategorian poistaminen */
	
	if(isset($_GET['deleteid'])) {
		
		$poisto = strip_tags($_GET["deleteid"]);
		$tulos = $yhteys->prepare("SELECT * FROM category WHERE category_id='$poisto' and owner='$kayttaja_sessio'");
		$tulos->execute();
		$res = $tulos->get_result();
		$tulos -> close();
		
		if($res->num_rows !== 0) {
			
			$tulos = $yhteys->prepare("DELETE category FROM category LEFT JOIN note ON category.category_id = note.category WHERE category.category_id = '$poisto'");
			$tulos->execute();
			$tulos -> close();
		
			
		} else {
			
			?> <script> alert("Tuloksia ei löytynyt!"); </script> <?php
			
		}
			
	} 


?>
<main>

<h2>Kategoriat</h2>

<!-- Kategoriat ja kussakin kategoriassa olevien muistettavien asioiden lukumäärä haetaan tietokannasta. -->
<!-- Kategoriat ovat käyttäjäkohtaisia - eli useammalla käyttäjällä voi olla saman niminen, mutta ne ovat silti eri kategorioita. -->
<table id="category-list">
	<tr>
		<th>Kategorian nimi</th>
		<th>Asioita</th>
	</tr>
	<?php 
	
		if ($kayttaja_sessio !== "") {
	
			$tulos = $yhteys->prepare("SELECT * FROM category WHERE owner='$kayttaja_sessio'");
			$tulos->execute();
			$res = $tulos->get_result();
			$tulos -> close();
			
			while($row = mysqli_fetch_assoc($res)) {
			
				$nimi = $row['name'];	
				$tulos2 = $yhteys->prepare("SELECT category_id FROM category INNER JOIN note ON category.category_id = note.category WHERE category.owner='$kayttaja_sessio' and name='$nimi'");
				$tulos2->execute();
				$res2 = $tulos2->get_result();
				$maara = mysqli_num_rows($res2);
				$tulos2 -> close();
		
			?>	
				<tr>
				<td class="category-name"><?php echo $nimi;  ?></td>
				<td class="notes-in-category"><?php echo $maara; ?></td>
				<td><a class="delbutton" href="manage.php?deleteid=<?php echo $row['category_id']; ?>" onclick="return confirm('Tämä poistaa myös kaikki kategoriassa olevat muistettavat asiat! Oletko varma?');">Poista</a></td>
				</tr>
			<?php

			}
			
		} else {
			
			header("Location: index.php");
			
		}
	
	?>
</table>

<div id="add-category">
	<h3>Lisää uusi kategoria</h3>
	<form action="manage.php" method="post">
		<p><b>Nimi: </b><input type="text" name="add-category-name"></p>
		<p><input type="submit" name="add-category"></p>
		<?php echo $kategoria_virhe; ?>
		<?php echo $kategoria_onnistui; ?>
	</form>
</div>

</main>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>