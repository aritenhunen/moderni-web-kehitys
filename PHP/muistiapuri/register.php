<?php require_once("header.php"); ?>
<?php require_once("login.php"); ?>
<?php require_once("yhteys.php"); ?>

<?php

	$rekisteroitymis_virhe = "";
	$rekisteroitymis_onnistui = "";
	$kirjautuminen = "";
	$kayttajatunnus = "";
	$yhteys = yhteystietokantaan();
	$nimimerkki = "";
	$sahkoposti = "";
	
		 
if(isset($_POST["register-submit"])) { 
  
  $kayttajatunnus = $yhteys -> real_escape_string(strip_tags($_POST['username'])); 
  
  $nimimerkki = $yhteys -> real_escape_string(strip_tags($_POST['nickname'])); 
  
  $salasana1 = $yhteys -> real_escape_string(strip_tags($_POST['password1'])); 
  
  $salasana2 = $yhteys -> real_escape_string(strip_tags($_POST['password2'])); 
  
  $sahkoposti = $yhteys -> real_escape_string(strip_tags($_POST['email'])); 

  $salasana = MD5($salasana1);
 


if (($kayttajatunnus !== "") && ($nimimerkki !== "") && ($salasana1 !== "") && ($salasana2 !== "") && ($sahkoposti !== "")) {
	
	$sahkoposti = strlen($sahkoposti);

  $stmt = $yhteys->prepare("INSERT INTO user (username, nickname, password, email) VALUES (?, ?, ?, ?)");
  $stmt->bind_param("sssi", $kayttajatunnus , $nimimerkki, $salasana, $sahkoposti);
  
  
if ($salasana1 == $salasana2) {

$tulos = $yhteys->prepare("SELECT * FROM user WHERE username='$kayttajatunnus'");
$tulos->execute();
$res = $tulos->get_result();
$tulos -> close();
 $kayttaja = mysqli_fetch_assoc($res);
if ($kayttaja) {

	$rekisteroitymis_virhe = "<p class='virhe'>Käyttäjätunnus on olemassa!</p>";

} else {
	
	/* onnistui */ 
	$_GET['kirjautuminen'] = 1;
	$stmt->execute();
	
	$rekisteroitymis_onnistui = "<p id='onnistui'>Rekisteröityminen onnistui. Kirjaudu sisään.</p>";
	
	
}

} else {
	
	$rekisteroitymis_virhe = "<p class='virhe'>Salasanat eivät täsmää!</p>";
}
$stmt -> close();
} else {
	
	$rekisteroitymis_virhe = "<p class='virhe'>Kentät ovat tyhjät, täytä kentät!</p>";
}


  }
   /* GET parametri kirjautuminen */
 if(isset($_GET["kirjautuminen"]) && $_GET["kirjautuminen"] == "tyhja") {  
  
	$kirjautuminen = "<p class='virhe'>Kentät ovat tyhjät, täytä kentät!</p>";
  
 } else  if(isset($_GET["kirjautuminen"]) && $_GET["kirjautuminen"] == "vaarin") {  
 
	$kirjautuminen = "<p class='virhe'>Virheellinen käyttäjätunnus ja salasana!</p>";
 
 }
		
?>
<main>
<?php if(isset($_GET['kirjautuminen']))  {  ?>
<br>
	<div id="login-box">
	<h2>Kirjaudu sisään</h2>
		<div id="login-box-content">
			<form action="register.php?rekisteroityminen2" method="post">
				<p><span class="login-label">Tunnus: </span><input type="text" name="username"></p>
				<p><span class="login-label">Salasana: </span><input type="password" name="password"></p>
				<p><input type="submit" name="login-submit" value="Kirjaudu"></p>
			</form>
			<p class="footnote">Ei tunnuksia? <a href="register.php">Rekisteröidy!</a>
			</div>
		<?php echo $kirjautuminen; ?>
		<?php echo $rekisteroitymis_onnistui; ?>
		
		
		</div>

	

<?php } else { ?>

<h2>Rekisteröidy</h2>

<form action="register.php" method="post">
	<!-- Käyttäjätunnus ei saa olla jo tietokannassa esiintyvä. Kaikki kentät pitää täyttää. -->
	<!-- Mahdolliset virheilmoitukset tulee näyttää lomakkeen yhteydessä. Jos rekisteröityminen onnistuu, lomaketta ei näytetä,
	vaan ilmoitus "Rekisteröityminen onnistui. Kirjaudu sisään." ja samanlainen kirjautumislaatikko kuin sivupalkissa. -->
	<p><span class="register-label">Käyttäjätunnus: </span><input type="text" name="username" value="<?php echo $kayttajatunnus; ?>"></p>
	<p><span class="register-label">Nimi: </span><input type="text" name="nickname" value="<?php echo $nimimerkki; ?>"></p>
	<p><span class="register-label">Salasana: </span><input type="password" name="password1"></p>
	<p><span class="register-label">Salasana uudelleen: </span><input type="password" name="password2"></p>
	<p><span class="register-label">Sähköposti: </span><input type="email" name="email" value="<?php echo $sahkoposti; ?>"></p>
	<p><input type="submit" name="register-submit" value="Rekisteröidy"></p>
</form>
	
<?php echo $rekisteroitymis_virhe; ?>

<?php } ?>
</main>
<?php if(!isset($_GET['kirjautuminen'])) { ?>
<?php require_once("sidebar.php");?>
<?php }?>
<?php require_once("footer.php"); ?>