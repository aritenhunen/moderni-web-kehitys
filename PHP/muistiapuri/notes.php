<?php require_once("header.php"); ?>
<?php require_once("yhteys.php"); ?>
<?php
	
	$yhteys = yhteystietokantaan();
	$kayttaja = kayttaja();
	$arvo = "%";
	$not_done = "";
	$done = "";
	$all = "";
	$shared = "";
	$all2 = "";
	
	

	if ($kayttaja==true) {
		
		$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);
		$yhteys = yhteystietokantaan();
		
		
		if (isset($_GET['note-status']) && ($_GET['note-status']  == "not-done")) {
				
			$arvo = 0;
			$not_done = "selected";
					
		} else if (isset($_GET['note-status']) && ($_GET['note-status']  == "done")) {
				
			$arvo = 1;
			$done = "selected";
	
		} else {
			
			$all = "selected";
		}
		
		if(isset($_GET['filter-submit']) && ($_GET['note-category'] == "shared")) {
			
			$tulos = $yhteys->prepare("SELECT note.note_id, note.done, note.title, category.name, note.deadline, note.description, user.nickname, category.owner FROM (((note INNER JOIN share ON note.note_id = share.note_id) INNER JOIN category ON category.category_id = note.category) LEFT JOIN user ON user.username = category.owner) WHERE share.username = '$kayttaja_sessio' and note.done LIKE '$arvo'");
			$tulos->execute();
			$res = $tulos->get_result();
			$tulos -> close();
			$shared = "selected";
			
			if($res->num_rows == 0) {
				
				?> <script> alert("Muiden jakamia tietoja ei löytynyt!"); </script> <?php
			}
		
		} else {
	
			$tulos = $yhteys->prepare("SELECT note.note_id, note.done, note.title, category.name, note.deadline, note.description, user.nickname, category.owner FROM (((note LEFT JOIN share ON share.note_id = note.note_id AND share.username = '$kayttaja_sessio') LEFT JOIN category on category.category_id = note.category) INNER JOIN user ON category.owner = user.username AND category.category_id = note.category) WHERE (share.username = '$kayttaja_sessio' or category.owner = '$kayttaja_sessio') AND note.done LIKE '$arvo'");
			$tulos->execute();
			$res = $tulos->get_result();
			$tulos -> close();
			$all2 = "selected";
		
		
			
			if(($res->num_rows == 0) && (isset($_GET['filter-submit']))) {
				
				?> <script> alert("Tuloksia ei löytynyt!"); </script> <?php
			}
		
		}
?>
<main>

<div id="filter-bar">
	<!-- huolehdi lomaketta käsitellessä, että oikeat valinnat pysyvät valittuina -->
	<form action="notes.php">
		<select name="note-status">
			<option value="not-done"<?php echo $not_done; ?>>Tekemättömät</option>
			<option value="done"<?php echo $done; ?>>Tehdyt</option>
			<option value="all"<?php echo $all; ?>>Kaikki</option>
		</select>
		<select name="note-category">
			<option value="all"<?php echo $all2; ?>>Kaikki kategoriat</option><!-- joko vain omat (helpompi) tai myös muiden jakamat (vaikeampi) -->
			<!-- Hae tähän kategoriat tietokannasta. -->
			<option value="shared"<?php echo $shared; ?>>Muiden jakamat</option>
		</select>
		<input type="submit" name="filter-submit" value="Hae">
	</form>
</div>

<!-- Haetaan tietokannasta -->

<?php 

while($row = mysqli_fetch_assoc($res)) {
		
		$tehty = "";
		$paivamaara = date("d-m-Y ", strtotime($row['deadline']));
		
	if(($row['owner'] !== null) && ($row['title'] !== null) && ($row['name'] !== null) && ($row['deadline'] !== null) && ($row['description'] !== null)) {
		
	if ($row['done'] == 1) {
		
		$tehty = '<span class="note-done">Tehty</span>';
		
	}

?>
<div class="note">
<?php if($kayttaja_sessio !== $row['owner']) { ?>
	<span class="shared-by"><?php echo $row['nickname']; ?></span> <!-- jos tämä on jonkun muun käyttäjälle jakama -->
<?php } ?>
	<h3><a href="note-view.php?noteid=<?php echo $row['note_id']; ?>"><?php echo $row['title']; ?></a><?php echo $tehty; ?></h3>
	<p><b>Kategoria: </b><?php echo $row['name']; ?> || <b>Aikaraja: </b> <?php echo $paivamaara; ?></p>
	<p><?php echo $row['description']; ?></p>
	<!-- (Muistettavan asian tekstistä max 200 merkkiä.) -->
</div>	
	
<?php	
	}	
		

}

?>
</main>
<?php

	if(isset($_GET['delete2_value']) && ($_GET['delete2_value'] == "true")) {
		
			?> <script> alert("Muistettava asia poistettiin onnistuneesti!"); </script> <?php
		
	}


} else {
		
		header("Location: index.php");
		
	} 
?>
<?php
require_once("sidebar.php");
require_once("footer.php");
?>