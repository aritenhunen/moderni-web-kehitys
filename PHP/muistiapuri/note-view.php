<?php require_once("header.php"); ?>
<?php require_once("yhteys.php"); ?>
<?php

$muistilista_onnistui = "";

if (isset($_GET['noteid']) && ($_GET['noteid']  !== "")) {
	
	if((isset($_GET["update_value"]) && ($_GET["update_value"] == "true"))){
		
		$muistilista_onnistui = "<p id='onnistui'>Muistilistan päivittäminen onnistui!</p>";
		
	} else if((isset($_GET["share_value"]) && ($_GET["share_value"] == "true"))){
		
		$muistilista_onnistui = "<p id='onnistui'>Käyttäjän jakaminen onnistui!</p>";
		
	} else if((isset($_GET["delete_value"]) && ($_GET["delete_value"] == "true"))){
		
		$muistilista_onnistui = "<p id='onnistui'>Käyttäjän jakaminen poistettiin onnistuneesti!</p>";
	}
	
	$muistettavaID = strip_tags($_GET["noteid"]);
	$kayttaja = kayttaja();
	$tehty = "";
	
if ($kayttaja==true) {
		
function tarkistaKatseluOikeus($muistettavaID){

	$yhteys = yhteystietokantaan();
	$tarkistus = array();
	$kayttaja_sessio = strip_tags($_SESSION["kayttaja"]);	
	$tulos = $yhteys->prepare("SELECT share.username, note.note_id, note.done, note.title, category.name, note.deadline, note.description, category.owner, user.nickname FROM ((note LEFT JOIN share ON note.note_id = share.note_id LEFT JOIN category ON note.category = category.category_id) LEFT JOIN user ON user.username = category.owner) WHERE (share.username = '$kayttaja_sessio' OR category.owner = '$kayttaja_sessio') AND note.note_id = '$muistettavaID' LIMIT 1");
	$tulos->execute();
	$res = $tulos->get_result();
	
	if($res->num_rows !== 0) {
		
	while ($row= mysqli_fetch_array($res)){
		$tarkistus[] = $row;
	}
		return $tarkistus;
  
	} else {
		
		$tarkistus = false;
		
		return $tarkistus;
	}
}

	$muistettava = tarkistaKatseluOikeus($muistettavaID);

if(tarkistaKatseluOikeus($muistettavaID) === false) {

	?> <script> alert("Sinulla ei ole katseluoikeutta tähän muistettavaan asiaan!"); window.location.href="index.php";</script> <?php
	
} else {
	

foreach($muistettava as $arvo) {
   
   $paivamaara = date("d-m-Y ", strtotime($arvo['deadline']));
   
if ($arvo['done'] == 1) {
		
		$tehty = '<span class="note-done">Tehty</span>';
		
	}


?>

<main>
<!-- poimi GET-parametri ja hae tietokannasta vastaava muistettava asia -->
<!-- TARKISTA ETTÄ KÄYTTÄJÄ OMISTAA SEN TAI SE ON JAETTU HÄNELLE -->

<h2><?php echo $arvo['title']; ?> <?php echo $tehty; ?></h2>

<p><b>Kategoria: </b><?php echo $arvo['name']; ?> || <b>Aikaraja: </b> <?php echo $paivamaara; ?></p>

<p><?php echo $arvo['description']; ?></p>

<!-- vain omistajalle, vain jos jaettu jollekulle -->


<ul><!-- linkin parametrit: tämän muistettavan asian id ja kyseisen käyttäjän id -->

<?php 
$kayttaja_sessio2 = strip_tags($_SESSION["kayttaja"]);	


 if ($arvo['owner'] == $kayttaja_sessio2) { 

	$tulos = $yhteys->prepare("SELECT share.note_id, user.nickname, share.username FROM (share INNER JOIN user ON user.username = share.username) WHERE note_id = '$muistettavaID'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
if($res->num_rows !== 0) {	
	echo '<p><b>Jaettu</b></p>';	
		
while($row = mysqli_fetch_assoc($res)) {
	
?> <li>Käyttäjä <?php echo $row['nickname']; ?><a class="remove-share" href="note-modify.php?removeshare=<?php echo $row['note_id']; ?>&user=<?php echo $row['username']; ?>">Poista jakaminen</a></li> <?php
	
}
	}
 }

?>

</ul>
<?php if ($arvo['owner'] !== $kayttaja_sessio2) { ?>
<!-- vain jos toisen jakama -->
<p>Tämän muistettavan asian sinulle jakoi: <b><?php echo $arvo['nickname']; ?></b></p>
<?php } else { ?>
<!-- vain omistajalle -->
<div id="note-modify-buttons">
<a href="note-modify.php?modifynote=<?php echo $arvo['note_id']; ?>">Muokkaa</a>
	<a href="note-modify.php?deletenote=<?php echo $arvo['note_id']; ?>">Poista</a>
</div>
<?php } ?>
<?php echo $muistilista_onnistui;  ?>
</main>

<?php
   

 
}
}
	} else {
		
		?> <script> alert("Käyttäjä ei ole olemassa!"); window.location.href="index.php";</script> <?php
		session_destroy();
	}

} else {
	
?> <script> alert("Tapahtui virhe!"); window.location.href="index.php";</script> <?php	
	
}


require_once("sidebar.php");
require_once("footer.php");
?>