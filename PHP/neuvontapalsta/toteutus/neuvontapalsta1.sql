-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19.03.2020 klo 13:54
-- Palvelimen versio: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `neuvontapalsta`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `kategoria`
--

CREATE TABLE `kategoria` (
  `kategoria_id` int(11) NOT NULL,
  `nimi` varchar(50) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Vedos taulusta `kategoria`
--

INSERT INTO `kategoria` (`kategoria_id`, `nimi`) VALUES
(70, 'kategoria1'),
(71, 'kategoria2'),
(72, 'kategoria3'),
(73, 'kategoria4'),
(74, 'kategoria5'),
(75, 'kategoria6'),
(76, 'kategoria7'),
(77, 'kategoria8'),
(78, 'kategoria9'),
(79, 'kategoria10'),
(80, 'kategoria11'),
(81, 'kategoria12');

-- --------------------------------------------------------

--
-- Rakenne taululle `kayttaja`
--

CREATE TABLE `kayttaja` (
  `kayttaja_id` int(11) NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `sahkoposti` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `salasana` varchar(100) COLLATE utf32_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Vedos taulusta `kayttaja`
--

INSERT INTO `kayttaja` (`kayttaja_id`, `nimimerkki`, `sahkoposti`, `salasana`) VALUES
(61, 'ari', 'ari@gmail.com', '25d55ad283aa400af464c76d713c07ad');

-- --------------------------------------------------------

--
-- Rakenne taululle `kysymys`
--

CREATE TABLE `kysymys` (
  `kysymys_id` int(11) NOT NULL,
  `otsikko` varchar(50) NOT NULL,
  `sisalto` text NOT NULL,
  `paivamaara` date NOT NULL,
  `kysyjan_nimimerkki_id` int(11) NOT NULL,
  `kategoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `kysymys`
--

INSERT INTO `kysymys` (`kysymys_id`, `otsikko`, `sisalto`, `paivamaara`, `kysyjan_nimimerkki_id`, `kategoria_id`) VALUES
(77, 'keskustelu1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis lacus nec erat luctus, porta iaculis lorem pretium. Nullam ut nisi justo. Ut id accumsan arcu. Duis vitae turpis at augue finibus placerat. Maecenas ac turpis in felis viverra luctus quis nec est.', '2020-03-19', 61, 70),
(78, 'keskustelu2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis lacus nec erat luctus, porta iaculis lorem pretium. Nullam ut nisi justo. Ut id accumsan arcu. Duis vitae turpis at augue finibus placerat. Maecenas ac turpis in felis viverra luctus quis nec est.', '2020-03-19', 61, 70),
(79, 'keskustelu3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis lacus nec erat luctus, porta iaculis lorem pretium. Nullam ut nisi justo. Ut id accumsan arcu. Duis vitae turpis at augue finibus placerat. Maecenas ac turpis in felis viverra luctus quis nec est.', '2020-03-19', 61, 70),
(80, 'keskustelu4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis lacus nec erat luctus, porta iaculis lorem pretium. Nullam ut nisi justo. Ut id accumsan arcu. Duis vitae turpis at augue finibus placerat. Maecenas ac turpis in felis viverra luctus quis nec est.', '2020-03-19', 61, 70),
(81, 'keskustelu5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed facilisis lacus nec erat luctus, porta iaculis lorem pretium. Nullam ut nisi justo. Ut id accumsan arcu. Duis vitae turpis at augue finibus placerat. Maecenas ac turpis in felis viverra luctus quis nec est.', '2020-03-19', 61, 70);

-- --------------------------------------------------------

--
-- Rakenne taululle `vastaus`
--

CREATE TABLE `vastaus` (
  `sisalto` text NOT NULL,
  `paivamaara` date NOT NULL,
  `vastaajan_nimimerkki_id` int(11) NOT NULL,
  `kysymys_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `vastaus`
--

INSERT INTO `vastaus` (`sisalto`, `paivamaara`, `vastaajan_nimimerkki_id`, `kysymys_id`) VALUES
('vastaus1', '2020-03-19', 61, 77),
('vastaus2', '2020-03-19', 61, 77),
('vastaus3', '2020-03-19', 61, 77),
('vastaus4', '2020-03-19', 61, 77),
('vastaus5', '2020-03-19', 61, 77);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoria_id`);

--
-- Indexes for table `kayttaja`
--
ALTER TABLE `kayttaja`
  ADD PRIMARY KEY (`kayttaja_id`);

--
-- Indexes for table `kysymys`
--
ALTER TABLE `kysymys`
  ADD PRIMARY KEY (`kysymys_id`),
  ADD KEY `kysyjan_nimimerkki_id` (`kysyjan_nimimerkki_id`),
  ADD KEY `kategoria_id` (`kategoria_id`);

--
-- Indexes for table `vastaus`
--
ALTER TABLE `vastaus`
  ADD KEY `vastaajan_nimimerkki_id` (`vastaajan_nimimerkki_id`),
  ADD KEY `kysymys_id` (`kysymys_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `kayttaja`
--
ALTER TABLE `kayttaja`
  MODIFY `kayttaja_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `kysymys`
--
ALTER TABLE `kysymys`
  MODIFY `kysymys_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
