<?php 

$virheilmoitus = "";
$virhe_viesti_ilmoitus = "";
$totta = 0;
$haku_kategoria_virhe = "";
$haku_kategoria_onnistui = "";
$haku_kategoria_virhe2 = "";
$haku_kategoria_onnistui2 = "";
$keskustelu_haku = "";
$kategoria_luokka2 = "";
$arvo_luku = "";
$kategoria_haku = "";

require 'palvelin.php';


?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="tyylit.css">
   <meta charset="UTF-8">

</head>
<body>
<?php
/* Jos lyotyy kayttaja sessio niin jatketaan */
if(isset($_SESSION["kayttaja"])) {
	
	$kayttaja = $_SESSION["kayttaja"];
	
	$tulos = $yhteys->prepare("select nimimerkki from kayttaja where nimimerkki = '$kayttaja'");

	$tulos->execute();

	$res = $tulos->get_result();
		
	$tulos -> close();
/* tarkistetaan etta kayttaja on olemassa */
	if($res->num_rows !== 0)  {

/* Jos lyotyy keskustelu jatketaan */
if(!isset($_GET["keskustelu_virhe"])) {
	
/* tarkistetaan onko kayttaja lahettanyt mitaan jos on poistetaan sessio laheta */
if((!isset($_GET["keskustelu"])) && (!isset($_POST['laheta']))) {
	
	unset($_SESSION["laheta"]); 
	
}


?>


<div id="sisalto">	
<header>
<h2>Neuvontapalsta</h2>
<h4>Keskustelusivu</h4>
</header>
<div class="yla_palkit">
<form method="post">  
	<a href="keskustelu_sivu.php">Keskustelu</a>
	<a href="keskustelu_sivu.php?tietoa">Tietoja meistä</a>
	<a href="neuvontapalsta.php?kirjaudu=0">Kirjaudu ulos</a>
	<!-- Nama palkit piilotetaan jos viesti on lahetetty -->
	<?php if((!isset($_SESSION["laheta"])) && (!isset($_POST["laheta_vastaus"]))) { ?>
	
	<a href="javascript:history.go(-1)">Takaisin</a>
	<a href="javascript:history.go(+1)">Seuraava</a>
	<?php } ?>
	<p id="kirjaudu"><b>Olet kirjautunut</b>: <?php echo $kayttaja; ?></p>
	
</form>
</div>
<br>

<div id="teksti_sisalto">

<?php

	echo  '<script language="javascript" type="text/javascript">
		
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>';
/* Jos tama saa getin tietoa siirretaan kayttaja etusivulle */
if(!isset($_GET["tietoa"])) {
/* Jos tama saa getin keskustelu siirretaan kayttaja keskustelusivulle */
	if(isset($_GET["keskustelu"])) {
/* tarkistetaan onko keskustelu tyhja */		
		if($_GET["keskustelu"] !== "") {
		
		$keskustelu = strip_tags($_GET["keskustelu"]);
		
		echo '<form method="post" class="keskustelu">'; 
		
		echo '<div class="paaotsikko">';
		
		$tulos = $yhteys->prepare("select kysymys.otsikko, kysymys.sisalto, kysymys.paivamaara, kayttaja.nimimerkki from kysymys INNER JOIN kayttaja ON kysymys.kysyjan_nimimerkki_id = kayttaja.kayttaja_id where kysymys_id='$keskustelu'");

		$tulos->execute();

		$res = $tulos->get_result();
		
		$tulos -> close();
/* tarkistetaan etta tulostetaan ainiostaan yksi keskustelu */
	if($res->num_rows <= 1)  {
/* tarkistetaan etta keskustelu on olemassa */
	if($res->num_rows !== 0)  {
	
	while($row = mysqli_fetch_assoc($res)) {
			$paivamaara = date("d-m-Y ", strtotime($row['paivamaara']));
		echo '<h1>' . $row['otsikko'] . '</h1>';
		echo '<p>' . $row['sisalto'] . '<p>';
		echo '<div class="keskustelu_nimimerkki">' . 'Kirjoittaja: ' . $row['nimimerkki'] . '</div>';
		echo '<div class="keskustelu_paivamaara">' . 'Päivämäärä: ' . $paivamaara . '</div>';
		
	}
	
	} else {
		
		header("Location: keskustelu_sivu.php?keskustelu_virhe");  
		
	}
	
	} else {
		
		die("Tulostettiin useampi keskustelu");
		
	}
		echo '</div>';
		
		echo '<div class="vastaus_kentta">';
		
	/* tulostetaan taman keskustelun vastaukset */
		
		$tulos = $yhteys->prepare("SELECT vastaus.sisalto, vastaus.paivamaara, kayttaja.nimimerkki, vastaus.kysymys_id FROM ((vastaus INNER JOIN kayttaja ON kayttaja.kayttaja_id = vastaus.vastaajan_nimimerkki_id) INNER JOIN kysymys ON vastaus.kysymys_id = kysymys.kysymys_id) WHERE kysymys.kysymys_id = '$keskustelu' ORDER BY vastaus.paivamaara DESC");

		$tulos->execute();

		$res = $tulos->get_result();
		
		$tulos -> close();
	 
		$maara = mysqli_num_rows($res);
/* tarkistetaan onko taman keskustelun vastauksia olemassa */
	if($res->num_rows !== 0) {
 
  /* Alustetaan muuttujat */ 
	$summa = 0;
	$arvo = 0;
	$muuttuja = 0;
	$alku = 1; 
	$arvo3 = 1;
	$edellinen = "";
	$seuraava = "";	
	$seuraava2 = 1;	
/* luodaan keskustelun sivut */
	$maara = $maara / 4;
	
	$maara2 = round($maara + 0.25);
	
	if($maara2 == 0) {
		
		$maara2 = 1;
	}
	while($row = mysqli_fetch_assoc($res)) {
/* tarkistetaan onko tama alkuperainen sivu */		
	if(isset($_GET["keskustelu_sivu"]) && $_GET["keskustelu_sivu"] !== "") {
	
		$arvo3 = strip_tags($_GET["keskustelu_sivu"]);

		$summa = strip_tags($_GET["keskustelu_sivu"]) * 4;
		
		$arvo = $summa - 4;
	
	/* luodaan aina uusi sivu jos edellisen vastauksia on enemman kuin 4 */
			if ($alku < $summa + 1) {
	
			if ($muuttuja == $arvo) {
			$paivamaara = date("d-m-Y ", strtotime($row['paivamaara']));
					echo '<br>';
		
		echo '<div class="keskustelu_nimimerkki">' . 'Vastaaja: ' . $row['nimimerkki'] . '</div>';
		
		echo '<div class="keskustelu_paivamaara">' . 'Päivämäärä: ' . $paivamaara . '</div>';
		
		echo '<br>';
		
		echo '<p>' . $row['sisalto'] . '</p>';
		
		echo '<hr/>';
				
			$totta = 1;
		
			
			} else {
				
				$muuttuja = $muuttuja + 1;
	
			}
		
		} 
			
/* keskustelusivun vaihtamisen toiminaallisuus */	
		$luku12 = strip_tags($_GET["keskustelu_sivu"]) + 1;
		
		$seuraava = strip_tags($_GET["keskustelu_sivu"]);
	
		$seuraava2 = $seuraava;
	
	if ($maara > $seuraava) {
		
		
		if ($seuraava >= "1") {
		
	
		$seuraava = $luku12;
		
		}
	} 
		$edellinen = strip_tags($_GET["keskustelu_sivu"]);
	
		
	
	if ($edellinen > "1") {
		
		$edellinen = $_GET["keskustelu_sivu"] - 1;
		
	}
	} else {
		$paivamaara = date("d-m-Y ", strtotime($row['paivamaara']));
		echo '<br>';
		
		echo '<div class="keskustelu_nimimerkki">' . 'Vastaaja: ' . $row['nimimerkki'] . '</div>';
		
		echo '<div class="keskustelu_paivamaara">' . 'Päivämäärä: ' . $paivamaara . '</div>';
		
		echo '<br>';
		
		echo '<p>' . $row['sisalto'] . '</p>';
		
		echo '<hr/>';
		
	$_GET["keskustelu_sivu"] = 1;


	}	

			$alku++;
	}
	

	
	if (($totta == 0) && (isset($_GET["keskustelu_sivu"]) && $_GET["keskustelu_sivu"] !== 1)) {
	
	header("Location: keskustelu_sivu.php?keskustelu_virhe");     
	
	}
	
	echo '</div>';

	echo '<div class="vastaus_sisalto">';
		
	echo '<div id="ala_palkit">';
/* tulostetaan taman hetkisen sivun maara ja sivu palkit */
	echo "<a class='previous round' href='keskustelu_sivu.php?keskustelu=" . $keskustelu . "&keskustelu_sivu=" . $edellinen ."'>&#8249;</a>"; 
	echo "<p id='maara'>" . $seuraava2 . " / " . $maara2 . "</p>";
	echo "<a class='next round' href='keskustelu_sivu.php?keskustelu=" . $keskustelu . "&keskustelu_sivu=" . $seuraava ."'>&#8250;</a>";	
	echo '</div>';	
		 
	}	

/* vastaus lomake */
	echo '<br><br>	
	<textarea id="tekstikentta" rows="4" cols="50" maxlength="500" name="tekstikentta" onKeyDown="limitText(this.form.tekstikentta,this.form.sisalto,500);" 
onKeyUp="limitText(this.form.tekstikentta,this.form.sisalto, 500);">
</textarea><br>
<br>
Sinulla on <input readonly type="text" name="sisalto" size="3" value="500"> merkkiä jäljellä.</font>
    <br><br>
    <input type="submit" name="laheta_vastaus" value="Lähetä vastaus"> 
	<br><br>
    <span class="error">' . $virhe_viesti_ilmoitus . '</span>';
		
		echo '</div>';
		echo '</form>';
	
	} else {
		
		header("Location: keskustelu_sivu.php?keskustelu_virhe");
		
	}
/* Jos tama saa getin uusiviesti siirretaan kayttaja uusiviesti sivulle */	
	  } else if(isset($_GET["uusiviesti"])) {
/* uusiviesti lomake */
	echo '<form method="post" class="uusi">  
	<input type="text" name="otsikko" placeholder="Otsikko">
	<br><br>	
	<textarea id="tekstikentta" rows="4" cols="50" maxlength="500" name="tekstikentta" onKeyDown="limitText(this.form.tekstikentta,this.form.sisalto,500);" 
onKeyUp="limitText(this.form.tekstikentta,this.form.sisalto, 500);">
</textarea><br>
<br>
Sinulla on <input readonly type="text" name="sisalto" size="3" value="500"> merkkiä jäljellä.</font>
    <br><br>
    <input type="submit" name="laheta" value="Lähetä"> 
	<br><br>
    <span class="error">' . $virhe_viesti_ilmoitus . '</span>	
</form>';
	
	} else {
/* muuten tulostetaan kategoriat */		
		$kategoria = "";
		
		$sivusto = "";
		
		$kategoria_luokka22 = "";
/* tarkistetaan onko kayttaja tietyssa kategoriassa */	
	if(isset($_GET["kategoria"]) && $_GET["kategoria"] !== "") {
			
			$kategoria = strip_tags($_GET["kategoria"]);
			
	}
/* tarkistetaan onko kayttaja tietyssa kategorian sivulla */		
	if(isset($_GET["sivu"]) && $_GET["sivu"] !== "") {
		
		$sivusto = strip_tags($_GET["sivu"]);
	}
/* tarkistetaan onko kayttaja valinnut tietyn kategorian */	
	if(isset($_GET["kategoria_luokka"]) && $_GET["kategoria_luokka"] !== "") {
	
		$kategoria_luokka22 = strip_tags($_GET["kategoria_luokka"]);
		
	}
	echo '<div id="kategoria">'; 


	echo '<form method="post">';
	echo '<input type="text" class="kategoria_palkki" name="nimi" placeholder="Luo kategoria" value=""> 
	<input type="submit" name="kategoria" value="Luo">';
	echo $virheilmoitus; 
	echo '</form>';

/* naytetaan kategoria lista */	
	
	$tulos = $yhteys->prepare("select * from Kategoria");

	$tulos->execute();

	$res = $tulos->get_result();
	$maara = mysqli_num_rows($res);
/* tarkistetaan onko yhtakaan kategoriaa */
if($res->num_rows !== 0) {
  
	$summa = 0;
	$arvo = 0;
	$muuttuja = 0;
	$alku = 1; 
	$arvo3 = 1;
	$edellinen = "";
	$seuraava = "";	
	$seuraava2 = 1;	
/* kategorian sivun maara toiminaallisuus */
 $maara = $maara / 10;
	
	$maara2 = round($maara + 0.4);
	
	if($maara2 == 0) {
		
		$maara2 = 1;
	}
 
/* kategorian hakeminen */
 if(isset($_GET["hae"])) {
 
	$kategoria_haku = strip_tags($_GET["hae"]);
		
/* tarkistetaan onko haku tyhja */
	if ($kategoria_haku !== "") { 
	
	/* haetaan tietokannasta */
	
	$tulos = $yhteys->prepare("select * from Kategoria where (nimi like '%". $kategoria_haku ."%') LIMIT 10");

	$tulos->execute();

	$res2 = $tulos->get_result();
	
	$tulos -> close();
/* tarkistetaan loytyko tuloksia */
	if($res2->num_rows !== 0)  {
	
		$res = $res2;
/* tietokanta hakee maksimissaan 10 tulosta jos tama ylittyy niin tulostetaan 10 tulosta */
		$kategoria_haku_maara = mysqli_num_rows($res);
		
		if ($kategoria_haku_maara > 10) {

			$kategoria_haku_maara = 10;
		
		}		
		
		$haku_kategoria_onnistui2 = $kategoria_haku_maara . " haku tulosta löytyi!";
		
	} else {
		
		$haku_kategoria_virhe2 = "Tuloksia ei löytynyt!";
		$arvo_luku = 1;
		
	}
	} else if(isset($_GET['kategoria_haku'])) {
		
		$haku_kategoria_virhe2 = "Täytä kenttä!";
	}
 
	header('Cache-Control: no cache');
 }
  /* kategoriat */
  
while($row = mysqli_fetch_assoc($res)) {
		
	$kategoria_maara = $row['kategoria_id'];
			
	$tulos20 = $yhteys->prepare("SELECT * FROM kysymys where kategoria_id = '$kategoria_maara'");

	$tulos20->execute();

	$res20 = $tulos20->get_result();
	
	$tulos20 -> close();
	
	if($res20->num_rows !== 0) {
	 $kategoria_maara = mysqli_num_rows($res20);
	 
	 $kategoria_maara = " [" . $kategoria_maara . "]";
	 
	} else {

	$kategoria_maara = "";
	}	

		$tulosta = '<a class=' . 'kategoria_luokka' . $alku . ' href=keskustelu_sivu.php?hae='.$kategoria_haku.'&kategoria_luokka='. $alku .'&kategoria='.$row['kategoria_id']. '&kategoria_sivu='. $arvo3 .'' .'&sivu=1' . '>'.$row['nimi'] . $kategoria_maara . '</a>';
	/* naytetaan taman hetkisen kategoria lista */	
			
	if(isset($_GET["kategoria_sivu"]) && $_GET["kategoria_sivu"] !== "") {
	
		$arvo3 = strip_tags($_GET["kategoria_sivu"]);

		$summa = strip_tags($_GET["kategoria_sivu"]) * 10;
	/* luodaan aina uusi kategoria lista jos edellisen kategoria listassa on enemman kuin 10 kategoriaa */
	
		$arvo = $summa - 10;
	
			if ($alku < $summa + 1) {
	
			if ($muuttuja == $arvo) {
	
			echo $tulosta;
			$totta = 1;
		
			
			} else {
				
				$muuttuja = $muuttuja + 1;
	
			}
		
		} 
			
	
	
	
		$luku12 = strip_tags($_GET["kategoria_sivu"]) + 1;
		
		$seuraava = strip_tags($_GET["kategoria_sivu"]);
/* kategorian listan vaihtamisen toiminaallisuus */
	$seuraava2 = $seuraava;
	
	if ($maara > $seuraava) {
		
		
		if ($seuraava >= "1") {
		
	
		$seuraava = $luku12;
		
		}
	} 
		$edellinen = strip_tags($_GET["kategoria_sivu"]);
	
		
	
	if ($edellinen > "1") {
		
		$edellinen = $edellinen - 1;
		
	}
	} else {

	echo $tulosta;
	$_GET["kategoria_sivu"] = 1;

	}	

			$alku++;
			
	

}


	if (($totta !== 0) || (isset($_GET["kategoria_sivu"]) && $_GET["kategoria_sivu"] == 1)) {

		if(isset($_GET['hae']) && ($_GET['hae'] !== "") && ($arvo_luku == ""))  {
			
			$seuraava = 1;
			$maara2 = 1;
			
		}
/* tulostetaan taman hetkisen kategoria listan maara ja sivu palkit */	
		echo '<div id="ala_palkit">';
	
echo "<a class='previous round' href='keskustelu_sivu.php?kategoria=" . $kategoria ."&kategoria_luokka=". $kategoria_luokka2 ."&kategoria_sivu=" . $edellinen ."&sivu=". $sivusto ."'>&#8249;</a>"; 
echo "<p id='maara'>" . $seuraava2 . " / " . $maara2 . "</p>";
echo "<a class='next round' href='keskustelu_sivu.php?kategoria=" . $kategoria ."&kategoria_luokka=". $kategoria_luokka2 ."&kategoria_sivu=" . $seuraava ."&sivu=". $sivusto ."'>&#8250;</a>";	
echo '</div>';	
	
	} else {

echo '<span class="error">' .  "<div class='virhe_ilmoitus'>Kategoria sivua ei löytynyt. <a href='javascript:history.go(-1)'>Palaa takaisin</a> </div>" . '</span>'; 

	}	
	
/* kategoria haku */
echo '<form method="GET" action="keskustelu_sivu.php?haku='. $kategoria_haku .'&kategoria='. $kategoria .'&sivu='. $sivusto .'&kategoria_luokka='. $kategoria_luokka22 .'" class="kategoria_haku">  
	<input type="text" name="hae" placeholder="hae kategorioita">
	<input type="submit" name="kategoria_haku" value="Hae"> 
	<br><br>
    <span class="error">' . $haku_kategoria_virhe2 . '</span>
	 <span class="onnistui">' . $haku_kategoria_onnistui2 . '</span>
</form>';
	
	
echo '</div>';
	
	
	
				
} else {

echo '<span class="error">' .  "<div class='virhe_ilmoitus'> Kategorioita ei löytynyt. Luo uusi kategoria. </div>" . '</span>';
		
	 echo '</div>';
}	

/* jos tama saa getin kategoria siirretaan kayttaja tiettyyn kategoriaan */	
if(isset($_GET["kategoria"]) && $_GET["kategoria"] !== "") {
		 
	$kategoria_viestit = strip_tags($_GET["kategoria"]);
		 
	/* haetaan tietokannasta tietty kategoria */
	echo '<div id="keskustelu">'; 
		
	$tulos = $yhteys->prepare("SELECT kysymys.kysymys_id, kysymys.otsikko, kysymys.paivamaara, kayttaja.nimimerkki FROM ((kysymys INNER JOIN kategoria ON kysymys.kategoria_id=kategoria.kategoria_id) INNER JOIN kayttaja ON kayttaja.kayttaja_id = kysymys.kysyjan_nimimerkki_id) WHERE kategoria.kategoria_id = '" . $kategoria_viestit . "' ORDER BY kysymys.paivamaara DESC");

	$tulos->execute();

	$res = $tulos->get_result();
	
	$tulos -> close();
	$maara = mysqli_num_rows($res);
	

if($res->num_rows !== 0) {
		
	echo '<a class="uusiviesti" href="keskustelu_sivu.php?uusiviesti=' . $kategoria_viestit . '">Uusi viesti</a>';
	
	$summa = 0;
	$arvo = 0;
	$muuttuja = 0;
	$alku = 1;
	$totta = 0;
	
	$maara = $maara / 4;
	/* kategoria sivun maara */
	
	$maara2 = round($maara + 0.25);
	
	if($maara2 == 0) {
		
		$maara2 = 1;
	}
/* tarkistetaan onko keskustelu sivu tyhja */	
	if(isset($_GET["sivu"]) && $_GET["sivu"] !== "") { 
		
	$tulos13 = $yhteys->prepare("select nimi from kategoria where kategoria_id = '" . $kategoria_viestit . "'");

	$tulos13->execute();

	$res13 = $tulos13->get_result();
		
	$tulos13 -> close();
		
	$row13 = mysqli_fetch_assoc($res13);
/* tarkistetaan loytyko kategoria nimea */
	if($res13->num_rows !== 0) {
			echo "<h2 id='kategoria_nimi'>".  $row13['nimi'] . "</h2>";
	} else {
		
		die("Kategoria nimeä ei löytynyt!");
	}
		
/* keskustelu haku */
	 if(isset($_POST["keskustelu_haku"])) {
 
	$keskustelu_haku = strip_tags($_POST["hae"]);
/* jos haku ei ole tyhja */
	if ($keskustelu_haku !== "") { 
	
	$tulos = $yhteys->prepare("SELECT kysymys.kysymys_id, kysymys.otsikko, kysymys.paivamaara, kayttaja.nimimerkki FROM ((kysymys INNER JOIN kategoria ON kysymys.kategoria_id=kategoria.kategoria_id) INNER JOIN kayttaja ON kayttaja.kayttaja_id = kysymys.kysyjan_nimimerkki_id) WHERE kategoria.kategoria_id = '" . $kategoria_viestit . "' and (kysymys.otsikko like '%". $keskustelu_haku ."%')");

	$tulos->execute();

	$res2 = $tulos->get_result();
	
	$tulos -> close();
/* tarkistetaan loytyko tuloksia */
	if($res2->num_rows !== 0)  {
	
	$maara2 = 1;
	$res = $res2;
		
	$kategoria_haku_maara = mysqli_num_rows($res);
		
	if ($kategoria_haku_maara > 4) {

			$kategoria_haku_maara = 4;
		
	}		
		
		$haku_kategoria_onnistui = $kategoria_haku_maara . " haku tulosta löytyi!";
		
	} else {
		
		$haku_kategoria_virhe = "Tuloksia ei löytynyt!";
		
	}
	} else {
		
		$haku_kategoria_virhe = "Täytä kenttä!";
	}
		header('Cache-Control: no cache');
 }
	
	$seuraava = $_GET["sivu"];
			echo "<p id='maara'>" . $seuraava . " / " . $maara2 . "</p>";
	
	
	while($row = mysqli_fetch_assoc($res)) {
	
		$summa = strip_tags($_GET["sivu"]) * 4;

 /* luodaan aina uusi keskustelu sivu jos edellinen keskustelu sivu on enemman kuin 4 keskustelua */	
		$arvo = $summa - 4;
	
		$luku12 = strip_tags($_GET["sivu"]) + 1;
			
		$kysymys_id = $row['kysymys_id'];
		
		$tulos2 = $yhteys->prepare("SELECT * from vastaus WHERE kysymys_id = '$kysymys_id'");

		$tulos2->execute();

		$luku = $tulos2->get_result();
		
		$tulos2 -> close();
		 $luvut = mysqli_num_rows($luku);

			$paivamaara = date("d-m-Y ", strtotime($row['paivamaara']));
		
		$tulosta = '<a href=keskustelu_sivu.php?keskustelu='.$row['kysymys_id'].'>' . ' <div id="keskustelu_otsikko"> '. '<div class="pien_keskuttelu_otsikko"><h1> ' . $row['otsikko'] . '</h1> <p id="luvut">' . 'Vastattuja: ' . $luvut . '</p></div>' . '' . '<br>'  . 'Nimimerkki: ' . $row['nimimerkki'] . '<div class="paivamaara">' . 'Paivämäärä: '  . $paivamaara .  '</div>' . '</div></a>';

		
		if ($alku < $summa + 1) {
	
			if ($muuttuja == $arvo) {
			
			echo $tulosta;
			$totta = 1;
			
			
			} else {
				
				$muuttuja = $muuttuja + 1;
			}
		
		} 
	
			$alku++;
	}
	
	$luku50 = strip_tags($_GET["sivu"]);
	
	
	if ($maara > $luku50) {
		
		
		if ($luku50 >= "1") {
		
	
		
		$seuraava = $luku12;
		}
	} 
		$edellinen = $luku50;
	
		
	
	if ($edellinen > "1") {
		
		$edellinen = $edellinen - 1;
		
	}
	
		if ($totta !== 0) {
			
	$kategoria_sivu = "";
	$kategoria_luokka3 = "";
	
	
	if(isset($_GET["kategoria_sivu"]) && $_GET["kategoria_sivu"] !== "") {
			
		$kategoria_sivu = strip_tags($_GET["kategoria_sivu"]);
			
	}
	
	if(isset($_GET["kategoria_luokka"]) && $_GET["kategoria_luokka"] !== "") {
			
		$kategoria_luokka3 = strip_tags($_GET["kategoria_luokka"]);
			
	}

	
		/* keskustelu haku */
		
		$keskustelu_haku_id = $_GET['kategoria'];
		
			echo '<form method="post" action="keskustelu_sivu.php?kategoria_sivu='. $kategoria_sivu .'&kategoria_luokka='. $kategoria_luokka3 .'&kategoria=' . $keskustelu_haku_id .'&sivu=1" class="kategoria_haku">  
	<input type="text" name="hae" placeholder="hae kysymyksiä">
	<input type="submit" name="keskustelu_haku" value="Hae"> 
	<br><br>
    <span class="error">' . $haku_kategoria_virhe . '</span>
	 <span class="onnistui">' . $haku_kategoria_onnistui . '</span>
</form>';
	
	
		
	echo '<div id="ala_palkit">';
/* keskustelu sivun maara ja sivun vaihto palkit */
echo "<a class='previous round' href='keskustelu_sivu.php?kategoria_luokka=". $kategoria_luokka3 ."&kategoria=" . $kategoria_viestit . "&kategoria_sivu=" . $kategoria_sivu ."&sivu=" . $edellinen ."'>&#8249;</a>"; 
echo "<a class='next round' href='keskustelu_sivu.php?kategoria_luokka=". $kategoria_luokka3 ."&kategoria=" . $kategoria_viestit . "&kategoria_sivu=" . $kategoria_sivu ."&sivu=" . $seuraava ."'>&#8250;</a>";	
echo '</div>';
		} else {
			
		header("Location: keskustelu_sivu.php?keskustelu_virhe"); 
			
		}
	
	} else {
		
	echo '<span class="error">' .  "<div class='virhe_ilmoitus'>Keskustelu sivustoa ei löytynyt. <a href='javascript:history.go(-1)'>Palaa takaisin</a> </div>" . '</span>';
	}
	 
	

	
		} else {

		 
		  echo '<span class="error">' .  "<div class='virhe_ilmoitus'> Tuloksia ei löytynyt.  <a class='uusiviesti' href='keskustelu_sivu.php?uusiviesti=" . $kategoria_viestit . "'>Uusi viesti</a> </div>" . '</span>';
	
			
		}
		
		 echo '</div>'; 
	 } else {
		 
		 echo '<div id="keskustelu">'; 
		 
		  echo '<span class="error">' .  "<div class='virhe_ilmoitus'> Valitse kategoria. </div>" . '</span>';
		 
		 echo '</div>'; 
		 
	 }
	
	}	

  } else {
	  
	   ?> 
	 <div id="etusivu">
	 <h2>Tervetuloa Neuvontapalstaan</h2>
	 <h3>Tietoja meistä</h3>
	 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla laoreet bibendum velit, non feugiat velit molestie hendrerit. Aliquam vitae elit elementum, vestibulum enim sed, vulputate risus. Nunc lobortis quis orci et tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque libero turpis, imperdiet sit amet tellus id, accumsan euismod dui. Sed mattis velit eu tincidunt consequat. Suspendisse dictum urna quis interdum feugiat. Maecenas bibendum, eros sed tincidunt volutpat, nunc nisi dictum sapien, non convallis orci felis sit amet velit. Suspendisse imperdiet odio in libero euismod, non aliquam tortor sagittis. Aenean ac suscipit massa, at efficitur felis. Pellentesque et hendrerit sem, molestie porttitor odio. Nulla vulputate viverra magna, at vehicula erat malesuada at. Duis finibus convallis tellus, vulputate tempor elit bibendum et. Donec dictum sodales risus, eget ultricies nibh rhoncus id. Praesent at mollis lorem, ac placerat lacus. </p>
	 </div>
	 
	
	 <?php
	  
  } 
	  

?>  </div> <?php

/* luodaan uusi kategoria */

if(isset($_POST["kategoria"])) { 

	$kategoria_nimi = $yhteys -> real_escape_string(strip_tags($_POST['nimi']));
	
	
/* jos kategoria nimi ei ole tyhja jatketaan */	
if(isset($kategoria_nimi) && $kategoria_nimi !== "") { 
	
	$tulos = $yhteys->prepare("select * from Kategoria where nimi='$kategoria_nimi'");

	$tulos->execute();

	$res = $tulos->get_result();

	$tulos -> close();
/* tarkistetaan etta kategoria nimi ei ole olemassa */
if($res->num_rows !== 1) {
	
	$stmt = $yhteys->prepare("INSERT INTO kategoria (nimi) VALUES (?)");
	$stmt->bind_param("s", $kategoria_nimi);
	$stmt->execute();
	header_remove(); 
	header("Refresh:0");
	/* kategoria luonti onnistui ja sivu ladataan uudestaan */
	
} 

} 
}

?>

<footer>

  <p>Yhteystiedot: <a href="mailto:neuvontapalsta@example.com">neuvontapalsta@example.com</a>.</p>
   <a href="https://fi-fi.facebook.com/"><img src="kuvat/facebook.png" alt="facebook" height="42" width="42"></a> 
    <a href="https://twitter.com/?lang=fi"><img src="kuvat/twitter.png" alt="twitter" height="42" width="42"></a> 
</footer>
</div>
</body>
</html>

<?php

/* virhe ilmoitukset */

} else {
	
	 echo '<span class="error">' . "<div class='virhe_ilmoitus'> Keskustelua ei löytynyt! <a href='javascript:history.go(-1)'>Palaa takaisin</a> </div>" . '</span>';
	
}

} else {

 echo '<span class="error">' . "<div class='virhe_ilmoitus'> Käyttäjä ei ole olemassa! <a href='neuvontapalsta.php?kirjaudu=0'>Kirjaudu sisään</a> </div>" . '</span>';
}	


} else {
		 
		 echo '<span class="error">' . "<div class='virhe_ilmoitus'> Vastaamalla viesteihin sinun täytyy kirjautua sisään. <a href='neuvontapalsta.php?kirjaudu=0'>Kirjaudu sisään</a> </div>" . '</span>';
		
}
?>