<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="tyylit.css">
   <meta charset="UTF-8">
</head>
<body>
<div id="sisalto">	
<header>
<h2>Neuvontapalsta</h2>
</header>
<div class="yla_palkit">
<a href="neuvontapalsta.php?kirjaudu">Kirjaudu</a>
<a href="neuvontapalsta.php?rekisteroidy">Rekisteröidy</a>
<a href="neuvontapalsta.php?etusivu">Etusivu</a>
</div>
<br><br>

<div id="teksti_sisalto2">
<?php
$virhe_nimi = "";
$virhe_sahkoposti = "";
$virhe_salasana = "";
$virhe_salasana2 = "";
$sahkoposti = "";
$tulos = "";
$nimi = "";
$hyvaksy = "";
$salasana = "";

if(!isset($_COOKIE["kirjautumis_nimi"])) {
$_COOKIE["kirjautumis_nimi"] = "";
}

require 'palvelin.php';


	 if(isset($_SESSION["kayttaja"]))  {

	 
	 if ((!isset($_POST['kirjaudu'])) && (!isset($_POST['rekisteroidy']))) {
	 
	session_destroy(); 
	
	 }
	 }	 


/* kirjaudu */
 if(isset($_GET["kirjaudu"])) {
echo '<div id="kirjautumis_lomake">';
echo '<form method="post">  
  <p>Nimimerkki:</p> <input type="text" name="nimi" placeholder="nimimerkki tai sähköposti" value="' . $_COOKIE["kirjautumis_nimi"] . '">
  <span class="error">* ' . $virhe_nimi . '</span>
  <br>
  <p>Salasana:</p><input type="password" name="salasana1" autocomplete="on" placeholder="salasana">
    <span class="error">* ' . $virhe_salasana . '</span>
	  <br><br>
	    <input type="checkbox" name="hyvaksy">
		<label>Muista minut</label>
    <br><br>
    <input type="submit" name="kirjaudu" value="Kirjaudu sisään"> 
 <br><br>
 
    <span class="error">' . $tulos . '</span>	
</form>';
echo '</div>';
 } else if(isset($_GET["rekisteroidy"])) {

echo '<form method="post">  
  <p>Nimimerkki:</p> <input type="text" name="nimi" placeholder="nimimerkki" value="' . $nimi .'">
  <span class="error">* ' . $virhe_nimi . '</span>
  <br>
  <p>Sähköposti:</p> <input type="email" name="sahkoposti" placeholder="sähköpostiosoite" value="' . $sahkoposti .'">
  <span class="error">*' . $virhe_sahkoposti . '</span>
  <br>
  <p>Salasana:</p> <input type="password" name="salasana1" autocomplete="on" placeholder="salasana">
    <span class="error">* ' . $virhe_salasana . '</span>
	  <br>
	    <p>Toista salasana:</p> <input type="password" name="salasana2" autocomplete="on" placeholder="uudelleen salasana">
    <span class="error">* ' . $virhe_salasana2 . '</span>
	  <br><br>
    <input type="checkbox" name="hyvaksy2">
		<label>Hyväksy käyttöehdot</label>
	<br><br>
    <input type="submit" name="rekisteroidy" value="Rekisteröidy"> 
 <br><br>
    <span class="error">' . $tulos . '</span>		
</form>';
	 
 } else  {
	 
	 ?> 
	 <div id="etusivu">
	 <h2>Tervetuloa Neuvontapalstaan</h2>
	 <h3>Tietoja meistä</h3>
	 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla laoreet bibendum velit, non feugiat velit molestie hendrerit. Aliquam vitae elit elementum, vestibulum enim sed, vulputate risus. Nunc lobortis quis orci et tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque libero turpis, imperdiet sit amet tellus id, accumsan euismod dui. Sed mattis velit eu tincidunt consequat. Suspendisse dictum urna quis interdum feugiat. Maecenas bibendum, eros sed tincidunt volutpat, nunc nisi dictum sapien, non convallis orci felis sit amet velit. Suspendisse imperdiet odio in libero euismod, non aliquam tortor sagittis. Aenean ac suscipit massa, at efficitur felis. Pellentesque et hendrerit sem, molestie porttitor odio. Nulla vulputate viverra magna, at vehicula erat malesuada at. Duis finibus convallis tellus, vulputate tempor elit bibendum et. Donec dictum sodales risus, eget ultricies nibh rhoncus id. Praesent at mollis lorem, ac placerat lacus. </p>
	 </div>
	 
	 
	 <?php
	 
	 
 }
 
?>
</div>
<footer>

  <p>Yhteystiedot: <a href="mailto:neuvontapalsta@example.com">neuvontapalsta@example.com</a>.</p>
   <a href="https://fi-fi.facebook.com/"><img src="kuvat/facebook.png" alt="facebook" height="42" width="42"></a> 
    <a href="https://twitter.com/?lang=fi"><img src="kuvat/twitter.png" alt="twitter" height="42" width="42"></a> 
</footer>
</div>
</body>
</html>