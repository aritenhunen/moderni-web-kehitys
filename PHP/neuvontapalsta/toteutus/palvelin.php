<?php
// aloittaa session
session_start();
?>
<?php

 $yhteys = new mysqli('localhost','root','');
$yhteys -> set_charset("utf8");
mysqli_select_db($yhteys, 'neuvontapalsta');

/* neuvontapalsta */

/* kirjautumis ja rekistoroitymis lomakkeiden virhe ilmoitukset */

   if(isset($_POST["nimi"]) && $_POST["nimi"] == "") { 
  
	$virhe_nimi = "<div class='virhe_ilmoitus'>Anna nimimerkki!</div>";
  
   }
   
    if(isset($_POST["sahkoposti"]) && $_POST["sahkoposti"] == "") { 
  
	$virhe_sahkoposti = "<div class='virhe_ilmoitus'>Anna sähköpostiosoite!</div>";
  
   }
	
	if(isset($_POST["salasana1"]) && $_POST["salasana1"] == "") { 
  
	$virhe_salasana = "<div class='virhe_ilmoitus'>Anna salasana!</div>";
  
   }
   
   if(isset($_POST["salasana2"]) && $_POST["salasana2"] == "") { 
  
	$virhe_salasana2 = "<div class='virhe_ilmoitus'>Anna toinen salasana!</div>";
  
   }
/* jos on saatu kirjautumisen tiedot */
if(isset($_POST["kirjaudu"])) { 

/* tarkistetaan etta tiedot eivat ole tyhjat */
  if(isset($_POST["nimi"]) && $_POST["nimi"] !== "") { 
$nimi = strip_tags($_POST["nimi"]);
  }

  if(isset($_POST["salasana1"]) && $_POST["salasana1"] !== "") { 
$salasana = strip_tags($_POST["salasana1"]);
  }
  if(isset($_POST["hyvaksy"]) && $_POST["hyvaksy"] !== "") { 

$hyvaksy = strip_tags($_POST["hyvaksy"]);

  }
/* evasteet */
if(isset($hyvaksy) && $hyvaksy !== "") {

setcookie("kirjautumis_nimi", $nimi, time() + (86400 * 30), "/"); 

 $_COOKIE['kirjautumis_nimi'] = $nimi;
}

 
if ($nimi !== "") {
 
 if ($salasana !== "") {
$salasana = MD5($salasana);

$tulos = $yhteys->prepare("SELECT Kayttaja_id FROM Kayttaja WHERE (sahkoposti='$nimi' OR nimimerkki='$nimi') and salasana = '$salasana'");


$tulos->execute();

$res = $tulos->get_result();
$tulos -> close();

  if($res->num_rows == 1) {
  	
	/* kirjautuminen onnistui */
	 $tulos = "";
	  $_SESSION["kayttaja"] = $nimi;
	  header("Location: keskustelu_sivu.php");
	  
	  
  } else {
	  
	  $tulos = "<div class='virhe_ilmoitus'>Virheelinen käyttäjätunnus ja salasana!</div>";
  }
  	
 }
 
}else {

	$virhe_nimi = "<div class='virhe_ilmoitus'>Anna nimimerkki tai sähköpostiosoite!</div>";
  
   }
   
   

  }
/* jos on saatu rekistoroitymis tiedot */
  if(isset($_POST["rekisteroidy"])) { 
 
/* tarkistetaan etta tiedot eivat ole tyhjat */
  $nimi = $yhteys -> real_escape_string(strip_tags($_POST['nimi'])); 
  
  $sahkoposti = $yhteys -> real_escape_string(strip_tags($_POST['sahkoposti'])); 
  
  $salasana1 = $yhteys -> real_escape_string(strip_tags($_POST['salasana1'])); 
  
  $salasana2 = $yhteys -> real_escape_string(strip_tags($_POST['salasana2'])); 
  
  $salasana = MD5($salasana1);
 
  $stmt = $yhteys->prepare("INSERT INTO Kayttaja (nimimerkki, sahkoposti, salasana) VALUES (?, ?, ?)");
  $stmt->bind_param("sss", $nimi , $sahkoposti, $salasana);

if (($nimi !== "") && ($sahkoposti !== "") && ($salasana1 !== "") && ($salasana2 !== "")) {

  if(isset($_POST["hyvaksy2"]) && $_POST["hyvaksy2"] !== "") { 
/* jos salasana vahintaan 8 merkkia pitka */
if(strlen($salasana1) > 7) { 

/* tarkistetaan etta salasanat tasmaavat */
if ($salasana1 == $salasana2) {

	$tulos = $yhteys->prepare("SELECT * FROM Kayttaja WHERE nimimerkki='$nimi' OR sahkoposti='$sahkoposti' LIMIT 1");

	$tulos->execute();

	$res = $tulos->get_result();
	$tulos -> close();
	$kayttaja = mysqli_fetch_assoc($res);
/* tarkistetaan onko kayttaja olemassa */
if ($kayttaja) {
 if ($kayttaja['nimimerkki'] === $nimi) {
	 
	 $tulos = "<div class='virhe_ilmoitus'>Nimimerkki on olemassa!</div>";
	 
 }
 
  if ($kayttaja['sahkoposti'] === $sahkoposti) {
	 
	 
	 $tulos = "<div class='virhe_ilmoitus'>Sähköpostiosoite on olemassa!</div>";
 }
 
 if (($kayttaja['nimimerkki'] === $nimi) && ($kayttaja['sahkoposti'] === $sahkoposti)) {
 
		$tulos = "<div class='virhe_ilmoitus'>Nimimerkki ja sähköpostiosoite ovat olemassa!</div>";
 
 } 

} else {
	
	 /* rekisteröityminen onnistui */
	 $stmt->execute();
	 $tulos = "";
	 $_SESSION["kayttaja"] = $nimi;

	 
	header("Location: keskustelu_sivu.php");  
}
 
 	
} else {

$tulos = "<div class='virhe_ilmoitus'>Salasanat eivät täsmää!</div>";

}


} else {
	
$tulos = "<div class='virhe_ilmoitus'>Salasana pitää olla vähintään 8 merkkiä pitkä!</div>";	
	
}

  } else {
	  
	 $tulos = "<div class='virhe_ilmoitus'>Hyväksy käyttöehdot!</div>";	 
	  
  }

}


 	$stmt -> close();

}
/* keskustelu_sivu */

/* kategorian virhe ilmoitukset */
if(isset($_POST["kategoria"])) { 

$kategoria = strip_tags($_POST["nimi"]);

  if(isset($kategoria)) {
	  
	  if($kategoria == "") {
		  
	    $virheilmoitus = '<span class="error">' .  "<div class='virhe_ilmoitus'> Anna kategorian nimi! </div>" . '</span>';
				
	  } else {

	$tulos = $yhteys->prepare("select * from Kategoria where nimi='$kategoria'");

	$tulos->execute();

	$res = $tulos->get_result();


if($res->num_rows == 1) {
	
	$virheilmoitus = '<span class="error">' .  "<div class='virhe_ilmoitus'> Kategorian nimi on olemassa! </div>" . '</span>';

}

	$tulos -> close();
  }	

  }   
 header('Cache-Control: no cache');
  }
  /* luodaan uusi viesti */
  
if(isset($_POST["laheta"])) { 

	$otsikko = $yhteys -> real_escape_string(strip_tags($_POST['otsikko'])); 
  
	$sisalto = $yhteys -> real_escape_string(strip_tags($_POST['tekstikentta'])); 
  
	$paivamaara = date("Y-m-d") . "\n"; 
	
	 if((isset($otsikko) && $otsikko !== "") && (isset($sisalto) && $sisalto !== "")) { 
	
	$kayttaja = strip_tags($_SESSION["kayttaja"]);
  
	$tulos = $yhteys->prepare("select kayttaja_id from kayttaja where nimimerkki='$kayttaja' or sahkoposti='$kayttaja'");

	$tulos->execute();

	$res = $tulos->get_result();

	$tulos -> close();
	
	$kayttaja = mysqli_fetch_assoc($res);
 
if ($kayttaja) {
	
	$uusiviesti = strip_tags($_GET['uusiviesti']);
	
	$tulos = $yhteys->prepare("select kategoria_id from kategoria where kategoria_id = '$uusiviesti'");

	$tulos->execute();

	$res = $tulos->get_result();
	
	if($res->num_rows !== 0) {
  
	$stmt = $yhteys->prepare("INSERT INTO kysymys (otsikko, sisalto, paivamaara, kysyjan_nimimerkki_id, kategoria_id) VALUES (?, ?, ?, ?, ?)");
	$stmt->bind_param("sssii", $otsikko , $sisalto, $paivamaara, $kayttaja['kayttaja_id'], $uusiviesti);
	$stmt->execute();
	$stmt -> close();
	
	$kayttaja = $kayttaja['kayttaja_id'];
	$kategoria = $uusiviesti;

	$tulos = $yhteys->prepare("select kysymys_id from kysymys where kysyjan_nimimerkki_id = '$kayttaja' and kategoria_id = '$kategoria' and otsikko = '$otsikko' and sisalto = '$sisalto' and paivamaara = '$paivamaara'");

	$tulos->execute();

	$res = $tulos->get_result();

	if($res->num_rows !== 0) {
	
	$kayttaja_id = mysqli_fetch_assoc($res);
	 
	$tulos20 = $kayttaja_id['kysymys_id'];
	
	header("Location: keskustelu_sivu.php?keskustelu=" . $tulos20 . ""); 
	
		$_SESSION["laheta"] = "1";
	
	} else {
		
		 die("Sivun vienti epäonnistui!");
		
	}
	$tulos -> close();
	
	$sisalto = "";
	$otsikko = "";
	
	
	} else {
		
		 $virhe_viesti_ilmoitus = '<span class="error">' .  "<div class='virhe_ilmoitus'> Viestin lähetettäessä tapahtui virhe. Kategoria ei ole olemassa. </div>" . '</span>';
	}
} else {
	
	  die("Käyttäjä ei löytynyt!");
	
}


	 } else {
		 
		 $virhe_viesti_ilmoitus = '<span class="error">' .  "<div class='virhe_ilmoitus'> Täytä puuttuvat kentät! </div>" . '</span>';
		 
	 }
	 header('Cache-Control: no cache');
}

/* vastataan viestiin */


if(isset($_POST["laheta_vastaus"])) { 
	


	$sisalto = $yhteys -> real_escape_string(strip_tags($_POST['tekstikentta'])); 
	
	$keskustelu = strip_tags($_GET["keskustelu"]);
	
	$paivamaara = date("Y-m-d"); 

 if((isset($keskustelu) && $keskustelu !== "") && (isset($sisalto) && $sisalto !== "")) { 

	$kayttaja = strip_tags($_SESSION["kayttaja"]);
  
	$tulos = $yhteys->prepare("select kayttaja_id from kayttaja where nimimerkki='$kayttaja' or sahkoposti='$kayttaja'");

	$tulos->execute();

	$res = $tulos->get_result();
	
	 $arvo = mysqli_fetch_assoc($res);
	 
	 $tulos -> close();
	 
if ($arvo) {
	
	$stmt = $yhteys->prepare("INSERT INTO vastaus (sisalto, paivamaara, vastaajan_nimimerkki_id, kysymys_id) VALUES (?, ?, ?, ?)");
	$stmt->bind_param("ssii", $sisalto , $paivamaara, $arvo['kayttaja_id'], $keskustelu);
	$stmt->execute();
	$stmt -> close();
	

	
	header('Cache-Control: no cache');
	
	
} else {
	
	 die("Käyttäjä ei löytynyt!");
	
}

 } else {
		 
		 $virhe_viesti_ilmoitus = '<span class="error">' .  "<div class='virhe_ilmoitus'> Täytä puuttuva tekstikenttä! </div>" . '</span>';
		 
	 }
	 


}

/* kategoria palkit */
if(isset($_GET["kategoria_luokka"]) && $_GET["kategoria_luokka"] !== "") {

$kategoria_luokka = strip_tags($_GET["kategoria_luokka"]);

$kategoria_luokka2 = $kategoria_luokka;

$kategoria_luokka = ".kategoria_luokka" . $kategoria_luokka;


 if(isset($_GET["hae"]) && $_GET["hae"] !== "") {
	 
	$kategoria_luokka2 = ""; 

 }
?>

<style>

#kategoria > <?php echo $kategoria_luokka; ?> {
	
	 background-color: #64868c;
	color: #f2f2f2;
	
}

</style>

<?php
}




?>