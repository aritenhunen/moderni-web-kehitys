$(document).ready(function() {

/* navigointi palkit */ 
$("#sivupalkki").children().attr('class', 'kiinni'); 

$("#sivupalkki nav").click(function() {
	
$(this).toggleClass('kiinni auki');
	
});

	$('#sisalto li').click(function(){
		
		$('#sisalto li').removeClass('valittu');
		$('#sisalto div').removeClass('valittu');

		$(this).addClass('valittu');
		
		var tab = $(this).attr("data-tab")
	
		$("#tab"+tab).addClass('valittu');
	});
	
/* rekisteroitymis lomake */
$('#rekisteroityminen input').focus(function(){
	
	var salasana = $("#salasanakentta1").val();

	
	if (($(this).attr("id") == "salasanakentta1") && (salasana.length <= 7)) {
		
		$(".ohje").show();
		 
	}

$(":input[type='password']").bind("keyup change", function(e) {
  
	salasana = $("#salasanakentta1").val();
	var salasana2 = $("#salasanakentta2").val();
	   
	  if((salasana == salasana2) && (salasana !== "")) {
       	$("#salasanakentta2").css("background-color", "#bcff85");
    } else if (salasana2 !== "") {
      	$("#salasanakentta2").css("background-color", "#ffb8e5");
    }
		
	if (salasana.length > 7) {
		
		$("#salasanakentta1").css("background-color", "#bcff85");
		 $(".ohje").hide();
		      
	} else if (salasana !== "") {
		
		$("#salasanakentta1").css("background-color", "#ffb8e5");
		
	}
})

	  

});

	 
$('#rekisteroityminen input').blur(function(){

	if (($(this).attr("id") !== "hyvaksykayttoehdot") && ($(this).attr("value")) !== "Rekisteröidy") {
	if ($(this).val()) {
		
		$(this).parents("p").addClass("tayta");
	} else {
	
		$(this).parents("p").removeClass("tayta");
	}
	
	}
		
		$(".ohje").hide();
		if (!$("#salasanakentta1").val()) {
	
		$("#salasanakentta1").removeAttr("style");
		}
		
		if (!$("#salasanakentta2").val()) {
		$("#salasanakentta2").removeAttr("style");
		}
		
	 
	
	
});

	
	$('#hyvaksykayttoehdot').click(function (e) {
       if($(e.target).is(':checked'))
       {
           $(':input[type="submit"]').prop('disabled', false);
          $('#hyvaksykayttoehdot').parents("p").addClass("tayta");
       }
       else
       {
           $(':input[type="submit"]').prop('disabled', true); 
          $('#hyvaksykayttoehdot').parents("p").removeClass("tayta");
       }
    });
	
	
	
	
	
$(':input[type="submit"]').click(function(e) {

	var nimi = $("#nimimerkkikentta").val();
	var sahkoposti = $("#sahkopostikentta").val();
	var salasana = $("#salasanakentta1").val();
	var salasana_2 = $("#salasanakentta2").val();
   
	$( ".rlabel" ).parent("p").css( "color", "#ff0000" );
	$( ".rlabel" ).parent("p.tayta").css( "color", "#000000" );

if (nimi == '' || sahkoposti == '' || salasana == '' || salasana_2 == '' || salasana !== salasana_2 || salasana.length < 7){

	event.preventDefault();
	
}
});	
	/* laskenta taulukko */


 
 
$('#lisaarivi').click(function() {


	tekstikentta = $('<td></td><td></td><td></td><td></td><td></td>'),
    poisto = $('<td class="poisto"><button>Poista</button></td>'),
    sisalto = $("<tr />");	
	$( "#laskentataulukko" ).append(sisalto);
	sisalto.append(tekstikentta).append(poisto);
 
});	

$('#laskentataulukko').on("click", ".poisto", function(e) {
		
	e.preventDefault();
	$(this).parent().remove(); 

});
		
$('#laskentataulukko').on("dblclick", "td", function(e) {
		
	var nykyinen_elementti = $(this);
	var arvo = $(this).html();
		
	if (!$(this).hasClass('poisto')) {
		
		updateVal(nykyinen_elementti, arvo);	 
			   
	} 
  
		e.stopPropagation();
		
	})
	 
function updateVal(nykyinen_elementti, arvo) {
    
	$(document).off('click');
    $(nykyinen_elementti).html('<input class="teksti" type="text"/>');
    $(".teksti").focus();

$(".teksti").keyup(function (event) {
    
	if (event.keyCode == 13) {
		
		$(nykyinen_elementti).html($(".teksti").val());
		
    }
});

$(document).click(function () {

	$(nykyinen_elementti).html($(".teksti").val());

});
}	
/* arvaus peli */

$('#aloitusnappi').click(function() {
	
	$("#kortit td").removeAttr('style');
	$("td").children("img").attr("src", "kortti.png");
	$("#aloitusnappi").text("Aloita peli uudestaan.");
	$("#viesti").text("");
	$("#kortit").unbind(); 
	  
	var luku = 0;
	var arr = [];
	var edellinen = "";
	var yrityksia = 3;
	 
	$("#viesti").text("Yrityksiä jäljellä:" + yrityksia);
	
/* sekoitetaan taulukko */
while (arr.length < $('#kortit td').length) {
  
  var num = Math.floor(Math.random() * 12 + 1);
  arr.push(num);
  
arr = arr.filter( function( item, index, inputArray ) {
          
	return inputArray.indexOf(item) == index;
	
});

  if (edellinen !== arr.slice(-1)[0]) { 

	$('td:eq('+luku+')', 'table').children("img").attr("data-indeksi", arr.slice(-1)[0]);
 	luku = luku + 1;
	
  }
  
	edellinen = arr.slice(-1)[0];
  
}  

$('#kortit').on("click", "td", function(e) {

	var kuva = $(this);

	$(this).css("transform", "rotateY(180deg)");
   
	$(this).css("transition", "transform 0.6s");

	$(this).css("transform-style", "preserve-3d");
     

	 
setTimeout(function(){
	  
	  
	if ( $(kuva).children("img").attr('src') == 'kortti.png' ) {

	  
		if ( $(kuva).children("img").attr('data-indeksi') == '3' ) {
	  
			$(kuva).children("img").attr("src", "aurinko.png");
		
			$("#viesti").text("Yrityksiä jäljellä:" + yrityksia);
	  
		} else if ( $(kuva).children("img").attr('data-indeksi') == '2' ) {
	  
			$(kuva).children("img").attr("src", "sydanrikki.png");
			$("#kortit").unbind();
			$("#viesti").text("Hävisit!");
			
		} else if ( $(kuva).children("img").attr('data-indeksi') == '1' ) {
	  
			$("#kortit").unbind();
			$(kuva).children("img").attr("src", "sydan.png");
			$("#viesti").text("Voitit!");

		} else {
		
			yrityksia = yrityksia -1;
			$(kuva).children("img").attr("src", "tyhjakortti.png");
			$("#viesti").text("Yrityksiä jäljellä:" + yrityksia);
	}

	if ((yrityksia == 0) && $(kuva).children("img").attr('data-indeksi') !== '1' ) {
	   
	   $("#kortit").unbind(); 
	   $("#viesti").text("Käytit kaikki yrityksesi");
	
   } 
 
   }
}, 300);


});
});

});