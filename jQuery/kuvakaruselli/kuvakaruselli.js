$(document).ready(function() {

var valilehdet = $('#valilehdet span');
var viimeinen = valilehdet.length;
$('#karuselli > div:first-child').show();
$("span.valittu").css("background-color", "#9c9c9c");
var nykyinen = 1;


 /* kuvan vaihto */
function kuvanvaihto(nykyinen) {
	
	  $("span.valittu").removeAttr('style');
	
	var kuva = "#kuva";
 
	var kuva_indeksi = nykyinen;
 
	var tab = "#tab"
 
	var tab_indeksi = nykyinen;
  
	valilehdet.removeClass('valittu');
 
	$(tab+tab_indeksi).addClass('valittu'); 

	$(".kuva").css("display", "none");
 
	$(kuva+kuva_indeksi).fadeIn();
	
	$('#karuselli div').removeClass('valittu');
    
	$(kuva+kuva_indeksi).addClass('valittu'); 
	
	$("span.valittu").css("background-color", "#9c9c9c");
 
   }
   /* valilehdet */
valilehdet.click(function() {
	
    if ( !$(this).hasClass('valittu') ) {
		
		nykyinen = $(this).attr("data-kuva-indeksi")
		
		nykyinen = parseInt(nykyinen);
		
        kuvanvaihto(nykyinen);
		
        nollaa_ajastin();
    }
});

/* ajastin */ 
function ajastintapahtuma() {
	
    nykyinen === viimeinen ? nykyinen = 1 : nykyinen = nykyinen+1;

    kuvanvaihto(nykyinen);
}

	var ajastin = setInterval(function() { ajastintapahtuma(); },5000);


function nollaa_ajastin() {
	
    clearInterval(ajastin);
    ajastin = setInterval(function() { ajastintapahtuma(); },5000);
}
});

