$(document).ready(function() {
	
	/* tehtava 1 */
	$("li").click(function(e){
  	
	 $(this).children().toggle();

	
		e.stopPropagation();
		
    });
/* tehtava 2 */

$("#valikko1").on('change', function() {
	
     var arvo = $(this).children("option:selected").val();
	 
	 if (arvo =="auto") {
		 
		 $("#valikko2").html("<option>henkilöauto</option><option>pakettiauto</option><option>kuorma-auto</option>");
	 }
	 
	 if (arvo =="vene") {
		 
		 $("#valikko2").html("<option>soutuvene</option><option>purjevene</option><option>moottorivene</option>");
	 }

	  if (arvo =="lento") {
		 
		 $("#valikko2").html("<option>lentokone</option><option>suihkukone</option><option>kuumailmapallo</option><option>riippuliidin</option>");
	 }
	     
});
/* tehtava 3 */

$("#uusi").click(function() {
	
    $(".kentta:last").before('<div><input type="text"><button class="poista">Poista</button></div>');

});

$("#kentat").on("click", ".poista",function() {
 	
	$(this).parent().remove();

});
/* tehtava 4 */
$(window).on('load resize', function () {
	
    var elementti = $("#teht4").width();
	
	var Fonttikoko = elementti / 13;
	
	$("#teht4").css("font-size", Fonttikoko);

	$("#teht4").css("height", elementti);
	
});
/* tehtava 5 */ 
$("#teht5 input").focus(function(){

	$(this).parent().children(".ohje").show();

});

$("#teht5 input").blur(function(){
   
   $(this).parent().children(".ohje").hide();

});
/* tehtava 6 */

$("#vari").on('change', function() {

	var vari = (this.value);

$(".uloin div").click(function(e){
  	
	var luku = $(this).css("background-color", vari);
	e.stopPropagation();
		
});

});
/* tehtava 7 */

var nykyinen_luku = $("#jaljella").text();

$("#max-50").on('keyup', function(event) {

var maksimi_luku = $("#max-50").val().length;

var tulos = nykyinen_luku - maksimi_luku;


  $("#jaljella").html(tulos); 
 
  
});
/* tehtava 8 */

$(function() {
  var ajastin = null;
  var arvo = 0;

  $("#kaynnistalaskuri").click(function() {
    if (ajastin !== null) return;
    ajastin = setInterval(function() {
      $("#laskuri").val(++arvo);
    }, 1000);
  });

  $("#pysaytalaskuri").click(function() {
    clearInterval(ajastin);
    ajastin = null
  });
});

});