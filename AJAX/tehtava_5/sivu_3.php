<?php
$data = file_get_contents("http://localhost/Portfiliotehtava_1/sivu_1.php?viikko=21");
$xml = simplexml_load_string($data);
/* xml viikkon numero */
 ?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="tyylit.css">
   <meta charset="UTF-8">
</head>
<body>

<div id="sisalto">

<header>
<h2>Satuvaltakunnan tarinat</h2>
<h3>Uutisia Lumotusta maasta</h3>
</header>
<div class="viikko_otsikko">
<h4>Viikon sää - viikko <?php echo $xml->viikonnumero[0]->viikko; ?></h4>
</div>


<?php
/* xml viikkon sää */
?> <div id="taulukko"> <?php

foreach($xml->viikonsaa as $key => $value)
{
	
	?> <div class="sisalto"><?php
	
  foreach ($value as $key => $val) {

  
  ?> <div class="<?php echo $key; ?>">  
  
  <?php 
  
  if ($key == "paiva") {
	 
	$paivamaara = date("d-m-Y", strtotime($val));
	echo($paivamaara);  
	  
  } else if ($key == "saatila") {
	  
	?> <b>Säätila: </b><br/> <?php echo($val);
	  
  } else if($key == "tuulennopeus") {
	  
	?> <b>Tuulennopeus: </b><br/> <?php echo($val . " m/s");
	 
  } else {
	  
	echo($val);
	
  }
 
  ?> 
  </div> <?php

}

?> </div> <?php

}


?>


</div>

<?php 
/* tietokanta taulu */

$yhteys = new mysqli('localhost','root','');
$yhteys -> set_charset("utf8");
mysqli_select_db($yhteys, 'uutissivusto');

$tulos = $yhteys->prepare("select * from uutiset ORDER BY uutiset.julkaisuaika DESC");

$tulos->execute();

$res = $tulos->get_result();

?> <div id="sisalto2"> <?php
			
?> <div id="otsikko"><h3>Uusimmat uutiset</h3></div> <?php

$summa = mysqli_num_rows($res);
$luku = 1;

while ($row = $res->fetch_assoc()) {
?> <div class="uutinen"><?php

	echo "<b>" . $row['otsikko'] . "</b>";

	$arvo = $row['julkaisuaika'];

	$paivamaara = date("d:m:Y ", strtotime($arvo));
	
	$aika = date("h:i", strtotime($arvo));

	echo("<p>" . $paivamaara . " klo " . $aika . "</p>");  

	if ($luku < $summa) {
		
		echo "<hr/>";
	} 
	
	$luku++;
	
?> </div> <?php
	  }	
$tulos->close();
/* blogi kirjoitus */

$data = file_get_contents ("http://localhost/Portfiliotehtava_1/sivu_2.php");
    
        $json = json_decode($data, true);


		?> <div id="otsikko"><h3>Vierailevat kirjoittajat</h3></div> <?php
	
        foreach ($json as $key => $value) {
         	?> <div class="blogi"><?php
			
				 echo '<div>'. '<b>' .$value['nimi']. '</b>' .": " .$value['otsikko'] .'</div>';
      
					$arvo = $value['julkaisuaika'];
	  
					$paivamaara = date("d:m:Y ", strtotime($arvo));

	  
				echo '<div id="ala_palkit">' . '<p>' .$value['kirjoittaja'] . '</p>' .'<p id="teksti">' . $paivamaara . '</p>' .'</div>';
				
				echo "<hr/>";
	  
				?> </div> <?php
            }


		
	  	?> </div> <?php
		
?>

<?php
/* tietokanta taulu 2 */
$tulos = $yhteys->prepare("SELECT * FROM uutiset WHERE uutinen_id IN(4, 5)");

$tulos->execute();

$res = $tulos->get_result();
	?> <div id="vasen"> <?php
while ($row = $res->fetch_assoc()) {
		  ?> <div class="<?php echo $row['kirjoittaja']; ?>">  
  
  <?php 
	echo "<h3>" . $row['otsikko'] . "</h3>";
	
	$arvo = $row['julkaisuaika'];

$paivamaara = date("d:m:Y ", strtotime($arvo));
$aika = date("h:i", strtotime($arvo));

	echo("<p class='kirja'>". $paivamaara . " klo " . $aika . " || " . $row['kirjoittaja'] . "</p>" ."<br/>");  

	echo "<p id='sisalto_teksti'>" . $row['sisalto'] . "</p>" . "<br/>";
	
?></div> <?php	
}

$tulos->close();
$yhteys->close();
?></div> <?php
?></div> <?php
?> 


</body>
</html>



