﻿<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Auton lisääminen ja poistaminen</title>
</head>

<body>

<?php
// sisällytetään tietokantaanyhdistämisfunktio
require("funktiot.php");

// muodostetaan yhteys tietokantaan
$yhteys = yhdista_tietokantaan();

// lisäyslomake on lähetetty, käsitellään tiedot
if(isset($_POST["lisays"]))
{
  // siirretään lomakkeen tiedot muuttujiin
  $rekisterinro = mysqli_real_escape_string($yhteys, $_POST["lisattava_rekisterinro"]);
  $vuosimalli = mysqli_real_escape_string($yhteys, $_POST["vuosimalli"]);
  $vari = mysqli_real_escape_string($yhteys, $_POST["vari"]);

  // muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
  $sql = "insert into auto (rekisterinro, vari, vuosimalli) values ('$rekisterinro', '$vari', $vuosimalli)";

  // suoritetaan kysely -- tällä kertaa ei haeta mitään, joten muuttuja $tulos on vain kyselyn onnistumisen tarkistamista varten
  $tulos = mysqli_query($yhteys, $sql);

  // jos lisäys onnistui, muuttujassa $tulos on arvo true; jos ei, niin false
  if($tulos)
  {
    echo "<p><b>Auto lisätty tietokantaan.</b></p>";
  }
  else
  {
    exit("Tietokantaoperaatio epäonnistui: " . mysqli_error($yhteys));
  }
}
// poistolomake on lähetetty, käsitellään tiedot
elseif(isset($_POST["poisto"]))
{
  // siirretään lomakkeen tiedot muuttujan
  $rekisterinro = mysqli_real_escape_string($yhteys, $_POST["poistettava_rekisterinro"]);

  // muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
  $sql = "delete from auto where rekisterinro='$rekisterinro';";

  // suoritetaan kysely -- tällä kertaa ei haeta mitään, joten muuttuja $tulos on vain kyselyn onnistumisen tarkistamista varten
  $tulos = mysqli_query($yhteys, $sql);

  // jos poisto onnistui, muuttujassa $tulos on arvo true; jos ei, niin false
  if($tulos)
  {
    echo "<p><b>Auto $rekisterinro poistettu tietokannasta.</b></p>";
  }
  else
  {
    exit("Tietokantaoperaatio epäonnistui: " . mysqli_error($yhteys));
  }
}
// mitään lomaketta ei ole lähetetty, näytetään lomake
else
{
?>

<h2>Lisää auto</h2>

<form action="autolomake.php" method="post">

<table border="0">
<tr>
  <td>Rekisterinumero:</td><td><input type="text" name="lisattava_rekisterinro"></td>
</tr>
<tr>
  <td>Vuosimalli:</td><td><input type="text" name="vuosimalli"></td>
</tr>
</table>

<tr>
  <td>Väri:</td>
  <td>
  <input type="radio" name="vari" value="sininen"> sininen
  <input type="radio" name="vari" value="punainen"> punainen
  <input type="radio" name="vari" value="keltainen"> keltainen
  <input type="radio" name="vari" value="musta"> musta
  <input type="radio" name="vari" value="harmaa"> harmaa
  <input type="radio" name="vari" value="kulta"> kulta
  </td>
</tr>

<p><input type="submit" value="Lisää" name="lisays"></p>

</form>

<h2>Poista auto</h2>

<form action="autolomake.php" method="post">

<p><b>Rekisterinumero: </b> <input type="text" name="poistettava_rekisterinro"><input type="submit" value="Poista" name="poisto"></p>

</form>

<?php
// muista else-haaraan liittyvä sulkeva aaltosulku
}
?>

</body>
</html>