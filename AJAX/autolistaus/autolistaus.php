﻿<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Tiedon hakeminen ja tulostaminen</title>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	
	$("a").click(function(){
  
	var varipohja = (this.id);
  
    $.post("autolistaus.php", {vari:varipohja}, function(data){
   
   $("#tulos").html(data);
	
    });
  });
});
</script>
<body>
<div id="tulos">

<?php

// sisällytetään tietokantaanyhdistämisfunktio
require("funktiot.php");

// muodostetaan yhteys tietokantaan
$yhteys = yhdista_tietokantaan();

// haetaan värin perusteella -- sivua on kutsuttu esimerkiksi "osoitteella" autot.php?vari=sininen, jolloin muuttujassa $_GET["vari"] on arvo "sininen"
if(isset($_POST["vari"]))
{
	
  // koodaa parametri turvalliseksi tietokantaa varten - tämä estää sekä tahattomia ongelmia syötteiden kanssa että hakkerointia

  
  $vari = mysqli_real_escape_string($yhteys, $_POST["vari"]);

  // muodostetaan SQL-kysely -- MUOKKAA TÄTÄ
  $sql = "select * from auto where vari='$vari' order by rekisterinro";
  
  // noudetaan kyselyn tulos muuttujaan - tässä muuttujassa ei siis ole vielä itse tietueita
  $tulos = mysqli_query($yhteys, $sql);
  
  // tarkistetaan, onnistuiko kysely
  if(!$tulos)
    exit("Tietokantahaku epäonnistui: " . mysqli_error());

  // tulostetaan taulukon aloittava tägi ja otsikkorivi -- käytä yksittäisiä lainausmerkkejä eli hipsuja, 
  // koska HTML-koodissa voi olla tavallisia lainausmerkkejä
  echo '<table border="1" cellpadding="5">';
  echo '<tr><td><b>Id</b></td><td><b>Rekisterinro</b></td><td><b>Väri</b></td><td><b>Vuosimalli</b></td></tr>';

  // noudetaan kyselyn seuraava rivi muuttujaan $auto ja jatketaan niin kauan kuin rivejä on
  while($auto = mysqli_fetch_assoc($tulos))
  {
    // taulukon yksi rivi eli yhden tietueen tiedot
    // MUOKKAA näitä echo-lauseita sen mukaan, millaista HTML:ää haluat tulostaa
    echo '<tr>';
	
	// HUOM! hakasuluissa olevat nimet vastaavat TIETOKANNAN kenttien nimiä, eivät muuttujia
	echo '<td>' . "autot" .'</td>';
	echo '<td>' . $auto["rekisterinro"] . '</td>';
	echo '<td>' . $auto["vari"] . '</td>';
	echo '<td>' . $auto["vuosimalli"] . '</td>';
	
	echo '</tr>';
  }
  // tulostetaan taulukon sulkeva tägi
  echo '</table>';
}

// sivua on kutsuttu lomakkeella, jossa method="post" ja submit-napin name="haku"
elseif(isset($_POST["haku"]))
{
  // haetaan rekisterinumeron tai sen osan perusteella
  $hakusana = "%" . mysqli_real_escape_string($yhteys, $_POST["rekisterinro"]) . "%";

  // käsky UPPER muuttaa hakusanan kokonaan suuraakkosiksi - vastaavasti LOWER muuttaisi sen pienaakkosiksi
  // like-operaatio yhtäsuuruusmerkin (=) sijaan hakee myös ne tulokset, joissa hakusana esiintyy osana rekisterinumeroa
  $sql = "select * from auto where rekisterinro like UPPER('$hakusana') order by rekisterinro";
  
  // taas noudetaan kyselyn tulos muuttujaan ja tarkistetaan onnistuiko
  $tulos = mysqli_query($yhteys, $sql);
  if(!$tulos)
    exit("Tietokantahaku epäonnistui: " . mysqli_error());

  // tulostetaan taulukon aloittava tägi ja otsikkorivi
  echo '<table border="1" cellpadding="5">';
  echo '<tr><td><b>Rekisterinro</b></td><td><b>Väri</b></td><td><b>Vuosimalli</b></td></tr>';

  // kuten edellä, noudetaan kyselyn seuraava rivi muuttujaan $auto ja jatketaan niin kauan kuin rivejä on
  while($auto = mysqli_fetch_assoc($tulos))
  {
    echo '<tr>';
	
	echo '<td>' . $auto["rekisterinro"] . '</td>';
	echo '<td>' . $auto["vari"] . '</td>';
	echo '<td>' . $auto["vuosimalli"] . '</td>';
	
	echo '</tr>';
  }
  // tulostetaan taulukon sulkeva tägi
  echo '</table>';
}
else
{
  // Sivua on kutsuttu ilman parametreja ja ilman lomaketta
  // Tulostetaan navigointilinkit ja lomake
  // SEURAAVA OSA tulee siis näkyviin vain jos mitään parametreja ei ole
?>


<p><a id="sininen" href="#">Siniset autot</a></p>
<p><a id="punainen" href="#">Punaiset autot</a></p>
<p><a id="keltainen" href="#">Keltaiset autot</a></p>
<p><a id="harmaa" href="#">Harmaat autot</a></p>


<form method="post">

<b>Rekisterinumero:</b> <input type="text" name="rekisterinro"> <input type="submit" name="haku">

</form>

<?php 
// muista else-haaraan liittyvä sulkeva aaltosulku
}
?>


</body>
</html>