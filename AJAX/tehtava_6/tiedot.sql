-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25.02.2020 klo 15:33
-- Palvelimen versio: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiedot`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `opiskelijoidentiedot`
--

CREATE TABLE `opiskelijoidentiedot` (
  `nimi` varchar(100) NOT NULL,
  `opiskelijanumero` int(50) NOT NULL,
  `sukupuoli` varchar(10) NOT NULL,
  `syntymaaika` varchar(10) NOT NULL,
  `tutkinto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vedos taulusta `opiskelijoidentiedot`
--

INSERT INTO `opiskelijoidentiedot` (`nimi`, `opiskelijanumero`, `sukupuoli`, `syntymaaika`, `tutkinto`) VALUES
('Olli Opiskelija', 12345678, 'mies', '12.3.1996', 'tieto- ja viestintätekniikan perustutkinto'),
('Minna Maalari', 27293267, 'nainen', '15.10.1998', 'rakennusalan perustutkinto'),
('Lauri Leipuri', 41729730, 'mies', '9.6.1997', 'hotelli-, ravintola- ja catering-alan perustutkinto');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `opiskelijoidentiedot`
--
ALTER TABLE `opiskelijoidentiedot`
  ADD PRIMARY KEY (`opiskelijanumero`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `opiskelijoidentiedot`
--
ALTER TABLE `opiskelijoidentiedot`
  MODIFY `opiskelijanumero` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41729731;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
