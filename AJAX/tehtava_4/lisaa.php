<?php 
$yhteys = new mysqli('localhost','root','');
$yhteys -> set_charset("utf8");

if($yhteys == false) {
    exit("Tietokantapalvelimeen yhdistäminen epäonnistui.");
}
if(!mysqli_select_db($yhteys, 'autokanta')) {
    exit("Tietokannan valinta epäonnistui.");
}

if((isset($_POST['vari']) && $_POST['vari'] !== "") && (isset($_POST['rekisterinumero']) && $_POST['rekisterinumero'] !== "") && (isset($_POST['vuosimalli']) && $_POST['vuosimalli'] !== "")) {
	
	$vari = $yhteys -> real_escape_string(strip_tags($_POST['vari'])); 
  
	$rekisterinumero = $yhteys -> real_escape_string(strip_tags($_POST['rekisterinumero'])); 
  
	$vuosimalli = $yhteys -> real_escape_string(strip_tags($_POST['vuosimalli'])); 
	
	$stmt = $yhteys->prepare("INSERT INTO auto (rekisterinro, vari, vuosimalli) VALUES (?, ?, ?)");
	$stmt->bind_param("sss", $rekisterinumero , $vari, $vuosimalli);
	
	$stmt->execute();
	$stmt -> close();
	
} 


?>