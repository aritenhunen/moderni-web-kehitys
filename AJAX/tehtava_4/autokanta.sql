-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09.05.2016 klo 19:48
-- Palvelimen versio: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autokanta`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `auto`
--

CREATE TABLE `auto` (
  `rekisterinro` varchar(10) NOT NULL,
  `vari` varchar(30) DEFAULT NULL,
  `vuosimalli` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `auto`
--

INSERT INTO `auto` (`rekisterinro`, `vari`, `vuosimalli`) VALUES
('ABC-123', 'punainen', '2010'),
('BUO-085', 'sininen', '2007'),
('CES-528', 'sininen', '2010'),
('HEH-654', 'harmaa', '2015');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`rekisterinro`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
