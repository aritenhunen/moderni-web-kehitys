<?php
	$yhteys = new mysqli('localhost','root','');
	$yhteys -> set_charset("utf8");

if($yhteys == false)
    exit("Tietokantapalvelimeen yhdistäminen epäonnistui.");
  
  if(!mysqli_select_db($yhteys, 'chathuone'))
    exit("Tietokannan valinta epäonnistui.");


if(isset($_GET['naytaviestit'])) {

	$tulos = $yhteys->prepare("SELECT * FROM viestit ORDER BY kellonaika DESC limit 20");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();
	
	while($row = mysqli_fetch_assoc($res)) {
		
		$paivamaara = date("d-m-Y", strtotime($row['kellonaika']));
		$aika = date("h:i", strtotime($row['kellonaika']));
			
			echo '<div class="kommentti">
				<p class="kommentoijainfo">Päivämäärä: '. $paivamaara .' Aika: '. $aika .'</p>
				<h2>'. $row['nimimerkki'] .'</h2>
				<div class="kommenttisisalto">
					<p>'. $row['teksti'] .'</p>
				</div>
			</div>';
			
			
	}

}

?>