<?php
	$yhteys = new mysqli('localhost','root','');
	$yhteys -> set_charset("utf8");

if($yhteys == false)
    exit("Tietokantapalvelimeen yhdistäminen epäonnistui.");
  
  if(!mysqli_select_db($yhteys, 'chathuone'))
    exit("Tietokannan valinta epäonnistui.");


	

if((isset($_POST['kommenttinimimerkki'])) && (isset($_POST['kommenttiteksti']))) {
	
	$kommenttinimimerkki = $yhteys -> real_escape_string(strip_tags($_POST['kommenttinimimerkki'])); 
	$kommenttiteksti = $yhteys -> real_escape_string(strip_tags($_POST['kommenttiteksti'])); 
	
	$paivamaara = date("Y-m-d H:i:s");
	
	if (($kommenttinimimerkki !== "") && ($kommenttiteksti !== "")) {
		
	$stmt = $yhteys->prepare("INSERT INTO viestit (teksti, nimimerkki, kellonaika) VALUES (?, ?, ?)");
	$stmt->bind_param("sss", $kommenttiteksti , $kommenttinimimerkki, $paivamaara);

	$stmt->execute();
	$stmt -> close();
		
	} 
	
	
} 
	
	
	
	
?>