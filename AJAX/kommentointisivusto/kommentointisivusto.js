$(document).ready(function(){
	
	var arvo = "";
/* naytetaan ensiksi asiakirjan pudotusvalikot */
	$.get("sivu_1.php", {asiakirjavalinta:arvo}, function(data) {

		$('#asiakirjavalinta').append(data); 
/* kun asiakirjan pudotusvalikot ovat valmiit naytetaan asiakirjan luettelo */		
			$.get("sivu_1.php", {asiakirjavalinta:arvo, luettelo:arvo}, function(data) {
				
				$(".luettelo").html(data);
/* tarkistetaan etta tietyn luettelon tekstimaara ei ylity 300 merkkia */
				function tekstimaara() {
				
				$('main > article > div').each(function post(index) {
				
				if($(this).text().length > 300) {
					
					data2 = $(this).text().substring(0, 300) + "...";
					
					var sisalto = '<p>' + data2 + '</p>';
					
					$(this).html(sisalto);
				}
	
			});
			
			
			}
	/* aloitetaan toiminto kun kayttaja on valinnut asiakirjan pudotusvalikosta */		
		$('#asiakirjavalinta').change(function( event, klikattu){ 
	/* tarkistetaan onko kayttaja jo valinnut asiakirjan */
		if(klikattu) {
			
			arvo = klikattu;
	
		} else {
		arvo = $(this).val();
		}
		
		$("#asiakirjavalinta option").removeAttr("selected"); 
		/* jos kaytta ei ole valinnut kaikki asiakirjat tulostetaan tietty asiakirja, muuten palataan aloitus sivulle */
		if((arvo !== "") && (arvo !== "Kaikki asiakirjat")) {

		/* valitaan taman hetkinen asiakirja pudotusvalikosta */
		$("#asiakirjavalinta").val(arvo).find("option[value=" + arvo +"]").attr('selected', true);
			
			$.get("sivu_1.php", {asiakirjavalinta:arvo}, function(data) {
		/* naytetaan taman asiakirjan sisalto */		
			$(".luettelo").html(data);
		
			$.get("sivu_2.php", {kommentti:arvo}, function(data) {
				
			$(".luettelo").append(data);
			
			/* laheta kommentti */
			$('#laheta-kommentti').click(function(){
				
			var kommenttinimimerkki = $("#kommenttinimimerkki").val();
			var kommenttiotsikko = $("#kommenttiotsikko").val();
			var kommenttiteksti = $("#kommenttiteksti").val();		
			
		/* tarkistetaan etta kentat eivat ole tyhjat */
			if((kommenttinimimerkki !== "") && (kommenttiotsikko !== "") && (kommenttiteksti !=="")) {
			
				$.post("sivu_3.php", {kommentti:arvo, kommenttinimimerkki:kommenttinimimerkki, kommenttiotsikko:kommenttiotsikko, kommenttiteksti:kommenttiteksti}, function(data) {
				
					$.get("sivu_2.php", {kommentti:arvo, vastaus:arvo}, function(data) {
							
						$("#kentta").html(data);
							
						$("#kommenttinimimerkki").val("");
						$("#kommenttiotsikko").val("");
						$("#kommenttiteksti").val("");	
						
					});
			
				});
				
			} else {
				
				alert("Tyhjät kentät, täytä kentät!");
				
			}
				
				
			});
	
		});
		
		});
			 
		} else {
/* naytetaan aloitus sivu ja kutsutaan tekstimaara funktiota tarkistamaan uudestaan luetteloiden tekstimaaran */
		
		$(".luettelo").html(data);
		
		tekstimaara();
/* hover toiminto */		
		$("h2").on({
		
	    mouseenter: function () {
	$(this).css("color", "tan");
    },
    mouseleave: function () {
     	$(this).css("color", "black");
    }
			});
	/* aloitetaan toiminto kun kayttaja on valinnut asiakirja otsikon */		
			$("h2").click(function() {
				
				var otsikko = this.id;
		
		 $('#asiakirjavalinta').trigger('change', [ otsikko ]);
		 
		});
		}
	
	});
/* sama toiminto kun edellinen mutta ennen kun on valinnut asiakirjan pudotusvalikosta */
		tekstimaara();	
		
		$("h2").on({
		
	    mouseenter: function () {
	$(this).css("color", "tan");
    },
    mouseleave: function () {
     	$(this).css("color", "black");
    }
			});
			
	$("h2").click(function() {
			
		var otsikko = this.id;
		
		 $('#asiakirjavalinta').trigger('change', [ otsikko ]);
		 
		});
		 
	
	});

	});

});


	
