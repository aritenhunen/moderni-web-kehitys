<?php

	$yhteys = new mysqli('localhost','root','');
	$yhteys -> set_charset("utf8");

if($yhteys == false)
    exit("Tietokantapalvelimeen yhdistäminen epäonnistui.");
  
  if(!mysqli_select_db($yhteys, 'kommentointisivusto'))
    exit("Tietokannan valinta epäonnistui.");

if(isset($_POST['kommentti'])) {
	
	$kommentti = $yhteys -> real_escape_string(strip_tags($_POST['kommentti'])); 
	$kommenttinimimerkki = $yhteys -> real_escape_string(strip_tags($_POST['kommenttinimimerkki'])); 
	$kommenttiotsikko = $yhteys -> real_escape_string(strip_tags($_POST['kommenttiotsikko'])); 
	$kommenttiteksti = $yhteys -> real_escape_string(strip_tags($_POST['kommenttiteksti'])); 
	$paivamaara = date("Y-m-d"); 
	
	if (($kommentti !== "") && ($kommenttinimimerkki !== "") && ($kommenttiotsikko !== "") && ($kommenttiteksti !== "")) {
	
	$stmt = $yhteys->prepare("INSERT INTO kommentti (otsikko, teksti, pvm, kommentoija, asiakirja_id) VALUES (?, ?, ?, ?, ?)");
	$stmt->bind_param("ssssi", $kommenttiotsikko , $kommenttiteksti, $paivamaara, $kommenttinimimerkki, $kommentti);

	$stmt->execute();
	$stmt -> close();
	}
}






?>