<?php
	$yhteys = new mysqli('localhost','root','');
	$yhteys -> set_charset("utf8");

if($yhteys == false)
    exit("Tietokantapalvelimeen yhdistäminen epäonnistui.");
  
  if(!mysqli_select_db($yhteys, 'kommentointisivusto'))
    exit("Tietokannan valinta epäonnistui.");


if((isset($_GET['asiakirjavalinta'])) || (isset($_GET['luettelo']))) {
	
	$arvo = "%";
	
	if(isset($_GET['asiakirjavalinta'])) {
	$asiakirja = strip_tags($_GET['asiakirjavalinta']);
	}

	if(isset($_GET['luettelo'])) {
		
		$luettelo = strip_tags($_GET['luettelo']);
	}
	
	if($asiakirja !== "") {
		
		$arvo = $asiakirja; 
		
	} 
	
	$tulos = $yhteys->prepare("SELECT * FROM asiakirja WHERE asiakirja_id LIKE '$arvo'");
	$tulos->execute();
	$res = $tulos->get_result();
	$tulos -> close();

	if($res->num_rows !== 0) {
		
	if ($asiakirja !== "") {
		
		$row = mysqli_fetch_assoc($res);
		
		$paivamaara = date("d-m-Y", strtotime($row['pvm']));
			
	$teksti = '<article>
			<h2>'. $row['otsikko'] .'</h2>
			
			<p class="kirjoittajainfo"><b>Kirjoittanut: </b>'. $row['kirjoittaja'] .' ('. $paivamaara .')</p>
			
			<p>'. $row['teksti'] .'</p>

		</article>';
		
	$teksti = str_replace("\r\n\r\n",'<p>', $teksti);
		
	echo $teksti;	
	} else if(!isset($luettelo)) {	
	while($row = mysqli_fetch_assoc($res)) {
		
		echo "<option value=". $row['asiakirja_id'] .">" . $row['otsikko'] . "</option>";
	
	}
		} else {
			
	while($row = mysqli_fetch_assoc($res)) {
		
		$paivamaara = date("d-m-Y", strtotime($row['pvm']));
			
		$teksti = '<article>
			<h2 id="'. $row['asiakirja_id'] .'">'. $row['otsikko'] .'</h2>
			<p class="kirjoittajainfo"><b>Kirjoittanut: </b>'. $row['kirjoittaja'] .' ('. $paivamaara .')</p>
			<div>
			<p>'. ($row['teksti']) .'</p>
			</div>
		</article>';
	
	$teksti = str_replace("\r\n\r\n",'<p>', $teksti);
	
	echo $teksti;
	
	}
		
		}
  
	} else {
		
		?> <script> alert("Asiakirjaa ei löytynyt!"); window.location.href="kommentointisivusto.html";</script> <?php
	}
  
} else {
	
	die("asiakirja valinta epäonnistui!");
}
  
?>